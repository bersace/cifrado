# -*- coding: utf-8 -*-

# Cifrado - Chiffrer et imprimer les messages de grand-jeu
# Copyright © 2006-2007 Étienne Bersac — Tous droits réservés.

import  gobject
import  gtk

import  gettext
_ = gettext.gettext

from    .       import  module
from    .       import  defs

def charger(el):
    return Force(el.getAttribute('nom').encode('utf-8'),
                 el.getAttribute('acteur'),
                 el.getAttribute('chef'),
                 el.getAttribute('clef'),
                 el.getAttribute('police'),
                 el.getAttribute('couleur'))

class Module(module.Module):
    def _init(self, cifrado):


        settings = gtk.settings_get_default()
        settings.set_property('gtk-alternative-button-order', True)

        d = self.ui.get_object('selecteur_police_dialog')
        d.add_buttons(gtk.STOCK_CLEAR, gtk.RESPONSE_NO)

        d.set_alternative_button_order([gtk.RESPONSE_CANCEL,
                                        gtk.RESPONSE_NO,
                                        gtk.RESPONSE_OK])

        self.forces    = gtk.ListStore(gobject.TYPE_PYOBJECT,
                                       gobject.TYPE_STRING,
                                       gobject.TYPE_STRING,
                                       gobject.TYPE_STRING,
                                       gobject.TYPE_STRING,
                                       gobject.TYPE_STRING)
        self.ui.get_object('forces_liste').set_model(self.forces)
        self.ui.get_object('em_dest_select').set_model(self.forces)


        liste = self.ui.get_object('forces_liste')
        # nom
        renderer = gtk.CellRendererText()
        renderer.set_property('editable', True)
        renderer.connect('edited', self.on_force_nom_edited)
        # traducteurs: titre de colonne
        colonne = gtk.TreeViewColumn(_('Nom'), renderer,
                                     text=defs.COL_FOR_NOM)
        colonne.set_expand(True)
        liste.append_column(colonne)

        # acteur
        renderer = gtk.CellRendererText()
        renderer.set_property('editable', True)
        renderer.connect('edited', self.on_force_acteur_edited)
        # traducteurs: titre de colonne
        colonne = gtk.TreeViewColumn(_('Acteur'), renderer,
                                     text=defs.COL_FOR_ACT)
        colonne.set_expand(True)
        liste.append_column(colonne)

        # chef
        renderer = gtk.CellRendererText()
        renderer.set_property('editable', True)
        renderer.connect('edited', self.on_force_chef_edited)
        # traducteurs: titre de colonne
        colonne = gtk.TreeViewColumn(_('Chef'), renderer,
                                     text=defs.COL_FOR_CHEF)
        colonne.set_expand(False)
        liste.append_column(colonne)

        # clef
        renderer = gtk.CellRendererText()
        renderer.set_property('editable', True)
        renderer.connect('edited', self.on_force_clef_edited)
        # traducteurs: titre de colonne
        colonne = gtk.TreeViewColumn(_('Clef par défaut'), renderer,
                                     text=defs.COL_FOR_CLEF)
        colonne.set_expand(False)
        liste.append_column(colonne)

        # police
        renderer = gtk.CellRendererText()
        # traducteurs: titre de colonne
        colonne = gtk.TreeViewColumn(_('Police'), renderer,
                                     text=defs.COL_FOR_POL,
                                     font=defs.COL_FOR_POL)
        colonne.set_expand(False)
        self.colonne_police = colonne
        liste.append_column(colonne)
        liste.connect("button-press-event", self.on_police_2button_press_event)

        # ÉMISSION
        combo = self.ui.get_object('em_dest_select')
        renderer = gtk.CellRendererText()
        combo.pack_start(renderer, False)
        combo.set_attributes(renderer, text=defs.COL_FOR_NOM)



    # gestion des forces
    def ajouter_force(self, force):
        self.forces.append([force, force.nom, force.acteur, force.chef, force.clef, force.police])

    def get_iter_force(self, force):
        return self.get_iter(self.forces, defs.COL_FORCE, force)

    def supprimer_force(self, force):
        iter = self.get_iter_force(force)
        del self.jeu.forces[force.nom]
        self.forces.remove(iter)

    def get_force_crt(self):
        liste = self.ui.get_object('forces_liste')
        sel = liste.get_selection()
        model, iter = sel.get_selected()
        if model is not None and iter is not None and model.iter_is_valid(iter):
            force = model.get_value(iter, defs.COL_FORCE)
        else:
            force = None
        return force
        


    def _ouvrir(self, jeu):
        for f in jeu.forces.values():
            self.ajouter_force(f)

    def _fermer(self):
        self.forces.clear()


    # CALLBACKS
    def on_force_nv_btn_clicked(self, bouton):
        nom = 'Force %i'%(len(self.jeu.forces))
        force = Force(nom)
        self.ajouter_force(force)
        self.jeu.forces[force.nom] = force
        self.sync(False)

    def on_force_nom_edited(self, renderer, path, new_text):
        iter = self.forces.get_iter(path)
        f = self.forces.get_value(iter, defs.COL_FORCE)

        if self.jeu.forces.has_key(new_text) and f.nom != new_text:
            self.cifrado.message(titre=_("Force existante !"),
                                 message=_("Il existe déjà une force « %s »")%(new_text))
            return

        del self.jeu.forces[f.nom]
        f.nom = new_text
        self.jeu.forces[f.nom] = f

        self.forces.set(iter, defs.COL_FOR_NOM, f.nom)
        # sûr ?
        self.cifrado.modules['message'].messages.foreach(self.force_nom_edited_msg_foreach)
        self.sync(False)


    def on_force_acteur_edited(self, renderer, path, acteur):
        iter = self.forces.get_iter(path)

        f = self.forces.get_value(iter, defs.COL_FORCE)
        f.acteur = acteur
        self.forces.set(iter, defs.COL_FOR_ACT, acteur)
        self.sync(False)

    def on_force_chef_edited(self, renderer, path, chef):
        iter = self.forces.get_iter(path)

        f = self.forces.get_value(iter, defs.COL_FORCE)
        f.chef = chef
        self.forces.set(iter, defs.COL_FOR_CHEF, chef)
        self.sync(False)


    def on_force_clef_edited(self, renderer, path, clef):
        iter = self.forces.get_iter(path)

        f = self.forces.get_value(iter, defs.COL_FORCE)
        f.clef = clef
        self.forces.set(iter, defs.COL_FOR_CLEF, clef)
        self.sync(False)

    def on_police_2button_press_event(self, liste, event):
        if event.type != gtk.gdk._2BUTTON_PRESS:
            return False

        path, colonne, x, y = liste.get_path_at_pos(int(event.x), int(event.y))

        if colonne is not self.colonne_police:
            return False

        iter = self.forces.get_iter(path)
        force = self.forces.get_value(iter, defs.COL_FORCE)
        
        d = self.ui.get_object('selecteur_police_dialog')

        # préselection de la police actuelle
        police = force.police
        if not police:
            police = 'serif 9'  # quelle est la vrai police par défaut ?
        d.set_font_name(police)
        res = d.run()
        d.hide()

        if res == gtk.RESPONSE_NO:
            police = ''
        elif res == gtk.RESPONSE_OK:
            police = d.get_font_name()
        else:
            return False

        self.sync(police == force.police)

        force.police = police
        self.forces.set_value(iter,defs.COL_FOR_POL, police)
        return True

    def force_nom_edited_msg_foreach(self, model, path, iter):
        msg = model.get_value(iter, defs.COL_MSG)
        model.set(iter,
                  defs.COL_MSG_EXP, msg.exp.nom)

    def on_force_suppr_btn_clicked(self, bouton):
        self.supprimer_force(self.get_force_crt())
        self.sync(False)
        



class Force:
    def __init__(self, nom="",acteur="",chef="",clef="",police="",couleur="#000000"):
        self.nom        = nom
        self.acteur     = acteur
        self.chef       = chef
        self.clef       = clef
        self.police     = police
        if couleur == "":
            couleur = "#000000"
        self.couleur    = couleur

    def get_gdk_color(self):
        return gtk.gdk.color_parse(self.couleur)

    def get_rgb(self):
        coul = self.get_gdk_color()
        comps = []
        for comp in [coul.red,coul.green,coul.blue]:
            comps.append(float(comp)/65635.)
        return comps

    def get_pixel(self):
        coul = self.get_gdk_color()
        r,g,b = coul.red>>8, coul.green>>8, coul.blue>>8
        return r << 24 | g << 16 | b << 8 | 0xff

    def createXML(self, doc):
        f = doc.createElement('force')
        f.setAttribute('nom',           self.nom)
        f.setAttribute('acteur',        self.acteur)
        f.setAttribute('chef',          self.chef)
        f.setAttribute('clef',          self.clef)
        f.setAttribute('police',        self.police)
        f.setAttribute('couleur',       self.couleur)
        return f

    def __str__(self):
        return "<Force %s 0x%x>"%(self.nom,id(self))
