# -*- coding: utf-8 -*-

# Cifrado - Chiffrer et imprimer les messages de grand-jeu
# Copyright © 2008 Étienne Bersac — Tous droits réservés.

import  gobject
import  gtk
from    gtk     import  gdk
import  pango
from    math    import  pi
import  cairo
import  tempfile
import  os,sys
import  re
import  time

import  gettext
_ = gettext.gettext

from    .       import  force
from    .       import  etape
from    .       import  lieu
from    .       import  message
from    .       import  module
from    .       import  imprimeur
from    .       import  defs

#traducteurs: Nom du calque commun à toute les cartes
COMMUN = _("Commun")

def charger(el, jeu):
    carte = Carte(jeu, el.getAttribute('image'),
                  int(el.getAttribute('resolution')),
                  el.getAttribute('police'))

    for e in el.getElementsByTagName('calque'):
        n = e.getAttribute('nom').encode('utf-8')
        if jeu.forces.has_key(n):
            n = jeu.forces[n]
        carte.nouveau_calque(n)

        for se in e.getElementsByTagName('position'):
            l = se.getAttribute('lieu')
            if not jeu.lieux.has_key(l):
                continue
            l = jeu.lieux[l]
            p = Position(l,float(se.getAttribute('x')),float(se.getAttribute('y')),se.getAttribute('image'))
            carte.poser(n, p)

    return carte

class CarteVue(gtk.Widget):

    __gsignals__ = {
        'lieu-pose':    (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE,
                         ()),
        'lieu-change':  (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE,
                         ())
        }
                         

    def __init__(self):
        gtk.Widget.__init__(self)

        self.set_carte(None)
        self.set_calque(None)
        self.set_pos(None)

        self.actif = False

    def set_carte(self, carte):
        self.carte = carte
        self.pixbuf = None

        if carte is None:
            return

        self.ech = ech = 100./float(carte.res)
        try:
            pixbuf = gtk.gdk.pixbuf_new_from_file(carte.abs_image)
        except:
            # Que faire ici ?
            return

        w = pixbuf.get_width()
        h = pixbuf.get_height()
        self.pixbuf = pixbuf.scale_simple(int(w*ech), int(h*ech),
                                          gtk.gdk.INTERP_BILINEAR)
        pixbuf = None

        self.calque_commun = COMMUN
        
        self.queue_resize()


    def set_calque(self, calque):
        self.calque = calque
        self.queue_draw()

    def set_pos(self, pos):
        self.pos = pos
        if self.carte is None:
            return

        self.queue_draw()

        if self.pos is None or self.calque is None:
            return


        # centrer sur la position courante
        x,y = self.pos.coords()

        # si la position n'a pas de coordonner, ne ignorer.
        if x+y <= 0:
            return

        x*=self.ech
        y*=self.ech
        sw = self.get_parent().get_parent()
        ha = sw.get_hadjustment()
        va = sw.get_vadjustment()
        hp = ha.get_property('page-size')
        vp = va.get_property('page-size')
        hu = ha.get_property('upper')
        vu = va.get_property('upper')
        sx = osx = ha.get_value()
        sy = osy = va.get_value()

        # "centrer" uniquement si invisible
        marge = 64
        if x < (sx + marge):
            sx=x-marge
        elif (sx + hp - marge) < x:
            sx=x-hp+marge
        sx=min(max(0., sx), hu-hp)

        if y < (sy + marge):
            sy=y-marge
        elif (sy + vp - marge) < y:
            sy=y-vp+marge
        sy=min(max(0., sy), vu-vp)

        # calcul de l'orientation du mouvement
        lx = sx-osx
        if lx == 0:
            ox = 0
        else:
            ox = lx/abs(lx)

        ly = sy-osy
        if ly == 0:
            oy = 0
        else:
            oy = ly/abs(ly)
        
        # calcul des incréments
        etapes = 5.
        etapex = abs(lx)/etapes
        etapey = abs(ly)/etapes

        # pseudo animation (aucun contrôle de temps :( )
        for i in range(int(etapes)+1):
            ha.set_value(osx+i*etapex*ox)
            va.set_value(osy+i*etapey*oy)

        

    def do_realize(self):
	self.set_flags(self.flags() | gtk.REALIZED)

	self.window = gdk.Window(
		self.get_parent_window(),
		width=self.allocation.width,
		height=self.allocation.height,
		window_type=gdk.WINDOW_CHILD,
		wclass=gdk.INPUT_OUTPUT,
		event_mask=self.get_events() | gdk.EXPOSURE_MASK
			| gdk.BUTTON1_MOTION_MASK
                        | gdk.BUTTON_PRESS_MASK
                        | gdk.BUTTON_RELEASE_MASK
			| gtk.gdk.POINTER_MOTION_MASK
			| gtk.gdk.POINTER_MOTION_HINT_MASK)

	self.window.set_user_data(self)
	self.style.attach(self.window)
        self.style.set_background(self.window, gtk.STATE_NORMAL)
        self.window.move_resize(*self.allocation)

    def poser(self, x, y):
        if self.pos is None:
            return

        self.pos.deplacer((x-self.offset_x)/self.ech,
                          (y-self.offset_y)/self.ech)

        self.queue_draw()
        self.emit('lieu-pose')

    def do_button_press_event(self, event):
	
	# make sure it was the first button
        if event.button == 3:
            self.set_pos(None)
        elif event.button == 1:
            self.chercher_pos(event.x, event.y)
            if self.pos:
                self.actif = True
                self.poser(event.x, event.y)

	return True

    def autour(self, val, repere):
        return repere-16 < val/self.ech and val/self.ech < repere+16

    def chercher_pos(self, x, y):
        pos = None
        calque = None
        for calque in self.carte.calques:
            pos = self.chercher_pos_calque(x, y, calque)
            if pos:
                break
            else:
                calque = None

        if pos:
            if calque is not self.calque:
                self.set_calque(calque)
            self.set_pos(pos)
            self.emit('lieu-change')
            return True

    def chercher_pos_calque(self, x, y, calque):
        for pos in self.carte.calques[calque].values():
            px, py = pos.coords()
            autour = self.autour(x-self.offset_x, px) and self.autour(y-self.offset_y, py)
            if autour:
                return pos

    def do_button_release_event(self, event):
        if event.button == 1:
            self.actif = False
            self.poser(event.x, event.y)

    def do_motion_notify_event(self, event):
        if not self.actif:
            return

        if event.is_hint:
            x, y, state = event.window.get_pointer()
        else:
            x = event.x
            y = event.y
            state = event.state

        if state & gdk.BUTTON1_MASK:
            self.poser(x, y)

            


    def do_unrealize(self):
        self.window.set_user_data(None)
	self.window.destroy()

    def do_size_request(self, requisition):
        if self.pixbuf:
            requisition.width       = self.pixbuf.get_width()
            requisition.height      = self.pixbuf.get_height()

    def do_size_allocate(self, allocation):
        if not self.pixbuf:
            return

        self.offset_x = (allocation.width - self.pixbuf.get_width())/2
        self.offset_y = (allocation.height - self.pixbuf.get_height())/2

        self.allocation = allocation

        if self.flags() & gtk.REALIZED:
            self.window.move_resize(*allocation)


        
    def do_expose_event(self, event):
        if self.pixbuf is None:
            return

        cr = self.window.cairo_create()
        cr.rectangle(self.offset_x, self.offset_y,
                     self.pixbuf.get_width(), self.pixbuf.get_height())
        cr.clip()
        cr.set_source_rgb(1.,1.,1.)
        cr.paint()

        self.window.draw_pixbuf(None, self.pixbuf,
                                0, 0,
                                self.offset_x, self.offset_y,
                                -1, -1,
                                gdk.RGB_DITHER_NONE, 0, 0)

        cr.scale(self.ech, self.ech)
        for calque in self.carte.calques:
            self.carte.dessiner_calque(cr, calque, [self.calque, self.pos], (self.offset_x/self.ech,self.offset_y/self.ech), True)

gobject.type_register(CarteVue)





class Module(module.Module):

    def _init(self, cifrado):

        self.calques = gtk.ListStore(gobject.TYPE_PYOBJECT,
                                     gobject.TYPE_STRING,
                                     gdk.Pixbuf)

        self.positions = gtk.ListStore(gobject.TYPE_PYOBJECT,
                                       gobject.TYPE_BOOLEAN,
                                       gobject.TYPE_STRING,
                                       gdk.Pixbuf)
                                   
        # BOÎTE D'OUVERTURE
        f = gtk.FileFilter()
        # traducteur: nom du filtre dans le selecteur de fond de carte
        f.set_name('Image PNG')
        for fmt in gtk.gdk.pixbuf_get_formats():
            for mt in fmt['mime_types']:
                if fmt['extensions'].__contains__('png'):
                    f.add_mime_type(mt)
        
        d = self.ui.get_object('select_fond_carte')
        d.add_buttons(gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL,
                      gtk.STOCK_OPEN,   gtk.RESPONSE_OK)
        d.add_filter(f)

        # CALQUES
        liste = self.ui.get_object('calques_liste')
        liste.set_model(self.calques)

        # nom
        renderer = gtk.CellRendererText()
        # traducteurs: titre de colonne
        colonne = gtk.TreeViewColumn(_('Force'), renderer,
                                     text=defs.COL_CALQUE_NOM)
        colonne.set_expand(True)
        liste.append_column(colonne)

        # couleur
        renderer = gtk.CellRendererPixbuf()
        # traducteurs: titre de colonne
        colonne = gtk.TreeViewColumn(_('Couleur'), renderer,
                                     pixbuf=defs.COL_CALQUE_COUL)
        colonne.set_expand(False)
        self.colonne_couleur = colonne
        liste.append_column(colonne)
        liste.connect("button-press-event", self.on_couleur_2button_press_event)

        # POSITIONS
        liste = self.ui.get_object('calque_positions_liste')
        liste.set_model(self.positions)

        # poser
        renderer = gtk.CellRendererToggle()
        renderer.set_radio(False)
        renderer.set_property('activatable', True)
        renderer.connect('toggled', self.on_position_toggled)
        # traducteurs: titre de colonne
        colonne = gtk.TreeViewColumn(_('Poser'), renderer,
                                     active=defs.COL_POS_POSER)
        colonne.set_expand(False)
        liste.append_column(colonne)

        # nom
        renderer = gtk.CellRendererText()
        # traducteurs: titre de colonne
        colonne = gtk.TreeViewColumn(_('Lieu'), renderer,
                                     text=defs.COL_POS_NOM)
        colonne.set_expand(True)
        liste.append_column(colonne)

        # CARTE
        self.vue = CarteVue()
        self.ui.get_object('carte_defileur').add_with_viewport(self.vue)
        self.vue.connect('lieu-pose', self.on_lieu_pose)
        self.vue.connect('lieu-change', self.on_lieu_change)

        # SIGNAUX
        cifrado.modules['force'].forces.connect('row-changed', self.forces_row_changed)
        cifrado.modules['force'].forces.connect('row-deleted', self.forces_row_deleted)
        cifrado.modules['lieu'].lieux.connect('row-changed', self.lieux_row_changed)
        cifrado.modules['lieu'].lieux.connect('row-deleted', self.lieux_row_deleted)

        sel = self.ui.get_object('calque_positions_liste').get_selection()
        sel.connect('changed', self.on_position_selected)

        sel = self.ui.get_object('calques_liste').get_selection()
        sel.connect('changed', self.on_calque_selected)

        self.wids = ['carte_cont',
                     'choisir_police_carte', 'supprimer_carte']

    def charger(self, carte):
        visible = carte is not None
        for wid in self.wids:
            self.ui.get_object(wid).set_sensitive(visible)

        self.calques.clear()

        pixbuf = gdk.Pixbuf(gdk.COLORSPACE_RGB,False,8,16,16)
        if carte is not None:
            # calque commun
            self.ajouter_calque(COMMUN)
            for f in self.jeu.forces.values():
                self.ajouter_calque(f)

        iter = self.calques.get_iter_first()
        if iter is not None:
            self.ui.get_object('calques_liste').get_selection().select_iter(iter)
        self.vue.set_carte(carte)
        self.ui.get_object('imprimer_carte').set_sensitive(visible)

    def get_calque_crt(self):
        model, iter = self.ui.get_object('calques_liste').get_selection().get_selected()

        if iter is None:
            return None

        c = self.calques.get_value(iter, defs.COL_CALQUE)
        return c

    def ajouter_calque(self, calque):
        pixbuf = gdk.Pixbuf(gdk.COLORSPACE_RGB,False,8,16,16)
        if calque == COMMUN:
            pixel = 0x000000FF
            nom = COMMUN
        else:
            pixel = calque.get_pixel()
            nom = calque.nom

        pixbuf.fill(pixel)
        
        self.calques.append([calque, nom, pixbuf])

    def supprimer_calque(self, calque):
        iter = self.get_iter(self.calques, defs.COL_CALQUE, calque)
        self.calques.remove(iter)

    def get_position_crt(self):
        calque = self.get_calque_crt()
        if calque is None:
            return calque, None

        sel = self.ui.get_object('calque_positions_liste').get_selection()
        model, iter = sel.get_selected()
        if iter is None:
            return calque, None

        lieu = model.get_value(iter, defs.COL_POS)

        return calque, lieu

    def ajouter_position(self, lieu):
        calque = self.get_calque_crt()
        pos = self.jeu.carte.calques[calque][lieu]
        poser = pos.pose()
        pb = gdk.Pixbuf(gdk.COLORSPACE_RGB,False,8,16,16)
        pb.fill(0xffffffff)
        self.positions.append([pos,poser,pos.lieu.nom,pb])

    def supprimer_position(self, lieu):
        iter = self.get_iter(self.positions, defs.COL_POS, lieu)
        if iter is not None:
            self.positions.remove(iter)

    # Interface Module
    def _ouvrir(self, jeu):
        self.charger(jeu.carte)

    def rel_image(self, image):
        if self.cifrado.fichier :
            print image, "devient",
            chemin, nom = os.path.split(self.cifrado.fichier)
            if image[0] != '/':
                image = os.path.normpath(chemin+'/'+image)
            prefix=""
            while image.find(chemin) == -1:
                chemin = os.path.dirname(chemin)
                prefix+="../"
            image = prefix+image.replace(chemin+"/", "")
        print image
        return image
        
    def sauver(self, fichier):
        if self.jeu.carte:
            self.jeu.carte.image = self.rel_image(self.jeu.carte.image)

    def _fermer(self):
        self.charger(None)

    # GESTIONNAIRES
    def on_charger_carte_clicked(self, bouton):
        d = self.ui.get_object('select_fond_carte')
        res = d.run()
        d.hide()

        if res == gtk.RESPONSE_OK:
            image = self.rel_image(d.get_filename())
            res = self.ui.get_object('resolution').get_value_as_int()

            if not self.jeu.carte:
                self.jeu.carte = Carte(self.jeu, image, res,'serif 12')
            else:
                self.jeu.carte.image = image
                self.jeu.carte.res = res

        if res == gtk.RESPONSE_CANCEL:
            return

        self.charger(self.jeu.carte)
        self.sync(False)

    def on_imprimer_carte_clicked(self, bouton):
        self.cifrado.imprimer(self.jeu.carte)

    def on_choisir_police_carte_clicked(self, bouton):
        d = self.ui.get_object('selecteur_police_dialog')
        d.set_font_name(self.jeu.carte.police)
        res = d.run()
        d.hide()

        if res == gtk.RESPONSE_NO:
            police = 'serif'
        elif res == gtk.RESPONSE_OK:
            police = d.get_font_name()
        else:
            return

        self.jeu.carte.police = police
        self.sync(False)
        self.vue.queue_draw()

    def on_supprimer_carte_clicked(self, bouton):
        res = self.cifrado.message(titre=_("Suppression la carte ?"),
                                   message=_("Êtes-vous sûr de vouloir "
                                             "supprimer la carte ? Cette "
                                             "action détruira toutes les "
                                             "coordonées des lieux sur "
                                             "chaques calques. L'assignation "
                                             "des couleurs est préservées."),
                                   buttons=gtk.BUTTONS_YES_NO)
        if res != gtk.RESPONSE_YES:
            return

        self.jeu.carte = None
        self.charger(self.jeu.carte)
        self.sync(False)

    def on_calque_selected(self, selection):
        self.positions.clear()
        calque = self.get_calque_crt()

        if calque is None:
            return

        self.vue.set_calque(calque)
        
        # conserver l'ordre de la liste des lieux
        lieux = self.cifrado.modules['lieu'].lieux
        iter = lieux.get_iter_first()
        while iter is not None and lieux.iter_is_valid(iter):
            lieu = lieux.get_value(iter, defs.COL_LIEU)
            self.ajouter_position(lieu)
            iter = lieux.iter_next(iter)

    def on_position_selected(self, selection):
        model, iter = selection.get_selected()
        if iter is None:
            pos = None
        else:
            calque, pos = self.get_position_crt()

        self.vue.set_pos(pos)
        
    def on_position_toggled(self, renderer, path):
        iter = self.positions.get_iter(path)
        calque = self.get_calque_crt()
        pos = self.positions.get_value(iter, defs.COL_POS)
        active = not renderer.get_active()

        if not active:
            pos.retirer()
            self.vue.set_pos(pos)
            
        self.positions.set_value(iter, defs.COL_POS_POSER, active)
        self.sync(False)

    def on_lieu_pose(self, vue):
        iter = self.get_iter(self.positions, defs.COL_POS, vue.pos)
        self.positions.set_value(iter, defs.COL_POS_POSER, True)
        self.sync(False)

    def on_lieu_change(self, vue):
        sel = self.ui.get_object('calques_liste').get_selection()
        if vue.calque:
            iter = self.get_iter(self.calques, defs.COL_CALQUE, vue.calque)
            pos = vue.pos
            sel.select_iter(iter)
            sel = self.ui.get_object('calque_positions_liste').get_selection()
            if pos:
                iter = self.get_iter(self.positions, defs.COL_POS, pos)
                sel.select_iter(iter)
            else:
                sel.unselect_all()
        else:
            sel.unselect_all()

        
    # maintenance des forces-calques/lieux-positions
    def forces_row_changed(self, forces, path, iter):
        if self.jeu is None or self.jeu.carte is None:
            return

        force = forces.get_value(iter, defs.COL_FORCE)
        # on incrémente pour tenir compte du calque commun.
        path = (path[0]+1,)
        try:
            iter = self.calques.get_iter(path)
            nom = self.calques.get_value(iter, defs.COL_CALQUE_NOM)
            self.calques.set_value(iter, defs.COL_CALQUE_NOM, force.nom)
        except:
            self.jeu.carte.nouveau_calque(force)
            self.ajouter_calque(force)

    def forces_row_deleted(self, forces, path):
        if self.jeu is None or self.jeu.carte is None:
            return

        for f in self.jeu.carte.calques:
            if f == COMMUN:
                continue
            else:
                fr = f

            if not self.jeu.forces.has_key(fr.nom):
                break
            else:
                fr = None

        if fr is None:
            return

        self.supprimer_calque(fr)
        self.jeu.carte.supprimer_calque(fr)


    def lieux_row_changed(self, lieux, path, iter):
        if self.jeu is None or self.jeu.carte is None:
            return

        lieu = lieux.get_value(iter, defs.COL_LIEU)
        # dans le doute …
        self.jeu.carte.nouveau_lieu(lieu)
        
        calque = self.get_calque_crt()
        if calque:
            try:
                iter = self.positions.get_iter(path)
                # màj
                self.positions.set_value(iter, defs.COL_POS_NOM, lieu.nom)
            except:
                self.ajouter_position(lieu)


    def lieux_row_deleted(self, lieux, path):
        if self.jeu is None or self.jeu.carte is None:
            return

        iter = self.positions.get_iter(path)
        lieu = self.positions.get_value(iter, defs.COL_POS)
        self.jeu.carte.supprimer_lieu(lieu)
        self.supprimer_position(lieu)


    def on_couleur_2button_press_event(self, liste, event):
        if event.type != gtk.gdk._2BUTTON_PRESS:
            return False

        path, colonne, x, y = liste.get_path_at_pos(int(event.x), int(event.y))

        if colonne is not self.colonne_couleur:
            return False

        iter = self.calques.get_iter(path)
        force = self.calques.get_value(iter, defs.COL_CALQUE)
        d = self.ui.get_object('selecteur_couleur')
        s = d.colorsel
        s.set_current_color(force.get_gdk_color())
        res = d.run()
        d.hide()

        if res == gtk.RESPONSE_OK:
            force.couleur = s.get_current_color().to_string()
            pixbuf = self.calques.get_value(iter, defs.COL_CALQUE_COUL)
            pixbuf.fill(force.get_pixel())
            self.sync(False)

        return True

    def on_picto_2button_press_event(self, liste, event):
        if event.type != gtk.gdk._2BUTTON_PRESS:
            return False

        path, colonne, x, y = liste.get_path_at_pos(int(event.x), int(event.y))

        if colonne is not self.colonne_picto:
            return False

        print "picto !"

        return True




class Position:
    def __init__(self, lieu, x=0, y=0, image=None):
        self.lieu = lieu
        self.deplacer(x, y)
        self.image = image

    def pose(self):
        return self.x > 0 and self.y > 0
    
    def retirer(self):
        self.deplacer(0,0)

    def deplacer(self, x, y):
        self.x = x
        self.y = y

    def coords(self):
        return (self.x, self.y)

    def dessiner(self, cr):
        pass

    def createXML(self, doc):
        el = doc.createElement('position')
        el.setAttribute('lieu', self.lieu.nom)
        el.setAttribute('x', str(self.x))
        el.setAttribute('y', str(self.y))
        el.setAttribute('image', str(self.image))

        return el

    def __str__(self):
        return "<Position de %s (%i,%i), %s>"%(self.lieu.nom, self.x,self.y, self.image)

class Carte(imprimeur.Imprimeur):
    def __init__(self, jeu, image, res, police):
        self.jeu        = jeu
        self.police     = police
        self.set_image(image, res)
        self.calques    = {}

        fs = jeu.forces.values()
        fs.append(COMMUN)

        for f in fs:
            self.nouveau_calque(f)

    def set_image(self, image, res):
        self.image      = image
        self.res        = res
        if image[0] != '/':
            self.abs_image  = os.path.join(self.jeu.dossier, self.image)
        else:
            self.abs_image = image

    def nouveau_calque(self, force):
        self.calques[force] = {}
        for lieu in self.jeu.lieux.values():
            self.nouveau_lieu(lieu)
       
    def supprimer_calque(self, force):
        del self.calques[force]

    def nouveau_lieu(self, lieu):
        for force in self.calques:
            if not self.calques[force].has_key(lieu):
                self.poser_lieu(force, lieu)

    def poser_lieu(self, force, lieu, x=0, y=0):
        self.poser(force, Position(lieu,x,y))

    def poser(self, force, pos):
        self.calques[force][pos.lieu] = pos
 
    def coord_lieu(self, force, lieu):
        return self.calques[force][lieu].coords()

    def supprimer_lieu(self, lieu):
        for calque in self.calques.values():
            if calque.has_key(lieu):
                del calque[lieu]

    def vide(self):
        return not len(self.image)>0
#         for f in self.jeu.forces.values():
#             if not self.calque_vide(f):
#                 print f, self.calque_vide(f)
#                 return False
#         return True

    def calque_vide(self, calque):
        for p in self.calques[calque].values():
            if p.pose():
                return False
        return True


    # impression
    def init_print(self, jeu):
        self.pages = {}

    def begin_print(self, op, contexte):
        # traducteurs: nom de travail d'impression
        op.set_job_name(_("Cartes grand-jeu « %s »")%(self.jeu.nom))
        self.print_settings.set("output-uri", self.jeu.nom+".pdf")

    def paginate(self, op, contexte):
        n = op.get_property('n-pages')
        if n < 0:
            n = 0

        duplex = op.get_print_settings().get_duplex() != gtk.PRINT_DUPLEX_SIMPLEX
        forces = []
        if len(self.jeu.messages):
            # faire un carte pour tout les destinaires de messages
            for m in self.jeu.messages:
                for e in m.emissions.values():
                    if e.env and not forces.__contains__(e.dest):
                        forces.append(e.dest)
       
        # compléter avec toute les forces ayant un calque dédié.
        for f in self.jeu.forces.values():
            if not self.calque_vide(f) and not forces.__contains__(f):
                forces.append(f)

        for f in forces:
            # traducteurs: nom de page pour suivi de l'impression
            titre = _("Carte « %s »")%(f.nom)

            # page de garde
            if duplex and n%2:
                n+=1
                op.titre_pages.append(titre)

            # carte
            self.pages[n] = f
            n+=1
            op.titre_pages.append(titre)
            # page blanche si duplex
            if duplex:
                self.pages[n] = None
                n+=1
                op.titre_pages.append(titre)

        if n > 0:
            op.set_n_pages(n)

        return True

    def draw_page(self, op, contexte, n, chef=False):
        if not chef and not self.pages[n]:
            return

        cr = contexte.get_cairo_context()
        cs = cairo.ImageSurface.create_from_png(self.abs_image)

        w = cs.get_width()
        h = cs.get_height()
        pw = contexte.get_width()
        ph = contexte.get_height()

        paysage = ((h/w) >= 1) != ((ph/pw) >=1)

        cr.save()
        if paysage:
            # passage en mode paysage
            cr.rotate(pi/-2)
            cr.translate(-1*ph, 0.)

            rw = h
            rh = w
            rpw = ph
            rph = pw
        else:
            rw = w
            rh = h
            rpw = pw
            rph = ph

        cr.save()

        # mise à l'échelle de la page (sans grossissement)
        ech_x = pw/rw
        ech_y = ph/rh
        ech = min(ech_x, ech_y)
        ech = min(ech, 1.)
        cr.scale(ech, ech)

        # centrage
        x = 0
        if rw*ech < pw:
            x = (pw-rw*ech)/2./ech
        y = 0
        if rh*ech < ph:
            y = (ph-rh*ech)/2./ech

        if paysage:
            t = x
            x = y     
            y = t

        cr.translate(x, y)

        # fond de carte
        cr.set_source_surface(cs, 0., 0.)
        cr.paint()

        # calques
        self.dessiner_calque(cr, COMMUN)
        if chef:
            for f in self.jeu.forces.values():
                self.dessiner_calque(cr, f,chef=True)
        else:
            f = self.pages[n]
            self.dessiner_calque(cr, f)

        # on se débarrasse de l'échelle et du centrage
        cr.restore()

        if not chef:
            # destinataire en haut à gauche
            fd = pango.FontDescription(self.police)
            layout = self.layout_text(contexte,f.nom,font=fd.to_string())
            w,h = imprimeur.csize(layout)
            self.rectangle(cr,0,0,w,h,(1.,1.,1.,.75),None,3)
            self.impr_layout(contexte,layout)
        else:
            i = 0
            # imprimer la légende
            th,tw = 0,0
            layouts = []
            for f in self.jeu.forces.values():
                if self.pages.values().__contains__(f):
                    ratio = 1.125   # et par rapport à ech ?

                    fd = pango.FontDescription(self.police)
                    taille = fd.get_size()
                    fd.set_size(int(taille/ratio))

                    layout = self.layout_text(contexte,f.nom,font=fd)
                    lw,lh = imprimeur.csize(layout)
                    th+=lh
                    tw = max(tw,lw)
                    i+=1
                    ly = rph-i*lh/ratio
                    layouts.append((f,layout,ly))
                    # dessin du fond blanc
                    self.rectangle(cr,0,ly,tw,th,(1.,1.,1.,.75),None,5)

            # dessin effectif de la légende au dessu
            for t in layouts:
                f,layout,y=t
                r,g,b= f.get_rgb()
                cr.set_source_rgb(r,g,b)
                self.impr_layout(contexte,layout,x=0,y=y)

        cr.restore()


    def dessiner_calque(self, cr, calque, cur=None,offset=None,chef=False):
        if cur is None:
            cur = [None, None]
        curcalque, curpos = cur

        if offset is None:
            offset = (0,0)

        for pos in self.calques[calque].values():
            x, y = pos.coords()
            if x == 0 or y == 0:
                continue

            x+=offset[0]
            y+=offset[1]

            rayon = 18
            focus =  pos is curpos and calque is curcalque

            if calque == COMMUN:
                nom = COMMUN
                r,g,b = [0., 0., 0.]
            else:
                nom = calque.nom
                if chef:        # ne colorer les carte que pour la
                                # carte de chefs.
                    r,g,b = calque.get_rgb()
                else:
                    r,g,b = (0.,0.,0.)
            # n'est pas le calque courant
            if curcalque is not None and calque is not curcalque:
                a = .75
            else:
                a = 1.

            if focus:
                a = 1.

            cr.set_source_rgba(r, g, b, a)

            dec = rayon/8.
            angle = pi/12.
            arcs = [[2, 4,      +1,     +1],
                    [8, 10,     -1,     +1],
                    [14,16,     -1,     -1],
                    [20,22,     +1,     -1]]

            for arc in arcs:
                cr.arc(x, y, rayon, arc[0]*angle, arc[1]*angle)
                cr.line_to(x+arc[2],y+arc[3])
                cr.fill()
            
            # dessiner l'étiquette
            layout = cr.create_layout()
            layout.set_text(pos.lieu.nom)
            fd = pango.FontDescription(self.police)
            layout.set_font_description(fd)
            layout.set_alignment(pango.ALIGN_CENTER)
            w, h = layout.get_size()
            w/=pango.SCALE
            h/=pango.SCALE
            x-=w/2
            y-=h+1.5*rayon

            # rectangle blanc en fond d'étiquette
            bordure=None
            if focus:
                bordure=(r,g,b,1.)
            self.rectangle(cr,x,y,w,h,(1.,1.,1.,a*.75),bordure,3)

            # dessin du texte lui même
            cr.set_source_rgba(r,g,b,a)
            cr.new_path()
            cr.move_to(x, y)
            cr.layout_path(layout)
            cr.fill()

    # sauvegarde
    def createXML(self, doc):
        c = doc.createElement('carte')
        c.setAttribute('image',         self.image)
        c.setAttribute('resolution',    str(self.res))
        c.setAttribute('police',        self.police)

        for f in self.calques:
            if isinstance(f, force.Force):
                n = f.nom
            else:
                n = f
            e = doc.createElement('calque')
            c.appendChild(e)
            e.setAttribute('nom', n)
            for pos in self.calques[f].values():
                e.appendChild(pos.createXML(doc))

        return c


    def __str__(self):
        return "<Carte %s at 0x%x>"%(self.jeu.nom,id(self))
