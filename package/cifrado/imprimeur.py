# -*- coding: utf-8 -*-

# Cifrado - un modéliseur de messages de grand-jeu
# Copyright © 2008 Étienne Bersac — Tous droits réservés.

from    math    import  pi
import  pango

import  gettext
_ = gettext.gettext

def pango_cairo(val):
    """traduit une valeur pango en valeur cairo"""
    return val/pango.SCALE

def csize(layout):
    """retourne la taille d'un layout pango en unité cairo"""
    return map(pango_cairo,layout.get_size())

def clsize(layouts):
    """calcule la taille d'une liste de layout en unité cairo"""
    tw = th = 0
    for i in layouts:
        w,h = csize(i[1])
        tw = max(w,tw)
        th+=h
    return tw,th

class Imprimeur:
    def init_print(self, jeu):
        pass

    def rectangle(self,cr,x,y,w,h,fond,bordure=None,marge=3):
        cr.save()
        cr.new_path()
        rayon = h/8+2*marge
        cr.arc(x+rayon-marge,     y+rayon-marge,  rayon,  pi,-pi/2.)
        cr.arc(x-rayon+marge+w,   y+rayon-marge,  rayon,  -pi/2.,0.)
        cr.arc(x-rayon+marge+w,   y-rayon+marge+h,rayon,  0.,pi/2.)
        cr.arc(x+rayon-marge,     y-rayon+marge+h,rayon,  pi/2.,pi)
        cr.close_path() 

        if fond:
            r,g,b,a = fond
            cr.set_source_rgba(r,g,b,a)
            cr.fill_preserve()

        if bordure:
            r,g,b,a = bordure
            cr.set_source_rgba(r,g,b,a)
            cr.stroke()

        cr.restore()

    def layout_text(self, contexte, text,
                    width=None,
                    align=pango.ALIGN_LEFT,
                    font='serif 8',
                    justify=False):
        """return layout"""
        if width is None:
            width = contexte.get_width()
        elif width < 0:
            width = contexte.get_width()+width

        layout = contexte.create_pango_layout()
        layout.set_width(int(width)*pango.SCALE)
        layout.set_wrap(pango.WRAP_WORD)
        layout.set_alignment(align);
        if not isinstance(font, pango.FontDescription):
            font = pango.FontDescription(font)
        layout.set_font_description(font)
        layout.set_text(text)
        layout.set_justify(justify)
        return layout

    def impr_layout(self, contexte, layout,x=0,y=0):
        cr = contexte.get_cairo_context()
        cr.save()
        cr.new_path()
        cr.move_to(x, y)
        cr.layout_path(layout)
        cr.fill()
        cr.restore()
        w,h = layout.get_size()
        return w/pango.SCALE, h/pango.SCALE

    def impr_text(self, contexte, text, x=0, y=0,
                  width=None,
                  align=pango.ALIGN_LEFT,
                  font='serif 8',
                  justify=False):
        """return layout size in pango unit"""
        layout = self.layout_text(contexte, text, width, align, font, justify)
        return self.impr_layout(contexte, layout,x,y)

    def layout_liste(self,contexte,items,width=None,font="serif 8"):
        layouts = []
        lh = 0
        for item in items:
            # traducteurs: cette chaîne est le préfixe des items de
            # liste. Choisissez le caractère correspondant au règle
            # typographiques de votre langue.
            l = self.layout_text(contexte=contexte, text=_("– %s")%(item),width=width,font=font)
            w,h = csize(l)
            layouts.append((lh,l))
            lh+=h
        return layouts

    def impr_liste(self, contexte, items, x=0,y=0,width=None,font='serif 8'):
        """Imprime une liste de texte"""
        layouts = self.layouts(contexte, items, width, font)
        lw = 0
        for layout in layouts:
            # traducteurs: cette chaîne est le préfixe des items de
            # liste. Choisissez le caractère correspondant au règle
            # typographiques de votre langue.
            w,h = self.impr_layout(contexte, layout[1],font=font,
                                    y=y+layout[0],x=x)
            lh+=h
            lw = max(lw,w)
        return lw, lh
