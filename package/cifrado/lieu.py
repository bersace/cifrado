# -*- coding: utf-8 -*-

# Cifrado - Chiffrer et imprimer les messages de grand-jeu
# Copyright © 2006-2007 Étienne Bersac — Tous droits réservés.

import  gobject
import  gtk

import  gettext
_ = gettext.gettext

from    .       import module
from    .       import defs

def charger(el):
    return Lieu(el.getAttribute('nom'))

class Module(module.Module):
    def _init(self, cifrado):
        self.lieux      = gtk.ListStore(gobject.TYPE_PYOBJECT,
                                        gobject.TYPE_STRING)
        self.ui.get_object('lieux_liste').set_model(self.lieux)
        self.ui.get_object('em_lieu_select').set_model(self.lieux)



        # onglets lieux
        liste = self.ui.get_object('lieux_liste')

        renderer = gtk.CellRendererText()
        renderer.set_property('editable', True)
        renderer.connect('edited', self.on_lieu_nom_edited)
        # traducteurs: titre de colonne
        colonne = gtk.TreeViewColumn(_('Nom'), renderer,
                                     text=defs.COL_LIEU_NOM)
        liste.append_column(colonne)
        # émission
        combo = self.ui.get_object('em_lieu_select')
        renderer = gtk.CellRendererText()
        combo.pack_start(renderer, False)
        combo.set_attributes(renderer, text=defs.COL_LIEU_NOM)


    # gestion des lieux
    def ajouter_lieu(self, lieu):
        self.lieux.append([lieu, lieu.nom])

    def get_iter_lieu(self, lieu):
        return self.get_iter(self.lieux, defs.COL_LIEU, lieu)

    def supprimer_lieu(self, lieu):
        iter = self.get_iter_lieu(lieu)
        del self.jeu.lieux[lieu.nom]
        self.lieux.remove(iter)

    def _ouvrir(self, jeu):

        for l in jeu.lieux.values():
            self.ajouter_lieu(l)


    def _fermer(self):
        self.lieux.clear()

    # LIEUX
    def on_lieu_nv_btn_clicked(self, bouton):
        nom = 'Lieu %i'%(len(self.jeu.lieux))
        lieu = Lieu(nom)
        self.ajouter_lieu(lieu)
        self.jeu.lieux[lieu.nom] = lieu
        self.sync(False)

    def on_lieu_nom_edited(self, renderer, path, new_text):
        iter = self.lieux.get_iter(path)
        l = self.lieux.get_value(iter, defs.COL_LIEU)

        if self.jeu.lieux.has_key(new_text) and l.nom != new_text:
            self.cifrado.message(titre=_("Lieu existant !"),
                                 message=_("Il existe déjà un lieu « %s »")%(new_text))
            return
            
        del self.jeu.lieux[l.nom]
        l.nom = new_text
        self.jeu.lieux[l.nom] = l
        self.lieux.set(iter, defs.COL_LIEU_NOM, l.nom)
        self.sync(False)

    def on_lieu_suppr_btn_clicked(self, bouton):
        tree, iter = self.ui.get_object('lieux_liste').get_selection().get_selected()
        if iter is not None:
            lieu = self.lieux.get_value(iter, defs.COL_LIEU)
            self.supprimer_lieu(lieu)
        self.sync(False)




        

class Lieu:
    def __init__(self, nom=""):
        self.nom        = nom

    def createXML(self, doc):
        l = doc.createElement('lieu')
        l.setAttribute('nom', self.nom)
        return l

    def __str__(self):
        return "<Lieu %s 0x%x>"%(self.nom,id(self))
