# -*- coding: utf-8 -*-

# Cifrado - Chiffrer et imprimer les messages de grand-jeu
# Copyright © 2006-2007 Étienne Bersac — Tous droits réservés.

import  gtk
from    ..      import    codeur

import  gettext
_ = gettext.gettext

def charger(node):
    codeur = Codeur(node.get_attribute('mot'))
    return codeur
    
class Codeur(codeur.Codeur):
    # traducteurs: nom d'un codeur chiffrant partiellement de
    # l'alphabêt. http://www.gentedelsur.cl/scout/murcielago.htm
    nom = _('Murcielago')

    def __init__(self, mot='murcielago'):
        self.mot = mot
        
    def convertir(self, text):
        mot = self.strip_special_chars(self.mot.lower())
        text = self.strip_special_chars(text)
        result = ''

        for char in text:
            if char.isalpha():
                try:
                    char = str(self.mot.index(char.lower()))
                except:
                    pass

                result+= char
            else:
                if char.isspace():
                    result+= char+' '
                else:
                    result+= char

        return result
    
    def get_prop_widget(self):
        box = gtk.HBox(False, 6)
        l = gtk.Label()
        # traduction: étiquette du champ de configuration de murcielago
        l.set_markup('<b>'+_('Mot clef :')+'</b>')
        box.pack_start(l, False, True, 0)
        entry = gtk.Entry()
        entry.set_text(self.mot)
        entry.connect('changed', self.on_entry_changed)
        box.pack_start(entry, True, True, 0)
        return box

    def get_prop_desc(self):
        if self.mot == 'murcielago':
            return ''
        else:
            return self.mot

    def sauver(self, doc, element):
        element.setAttribute('mot', self.mot)

    def on_entry_changed(self, entry):
        mot = entry.get_text()
        if len(mot) <= 10:
            self.mot = mot
        self.changed()
