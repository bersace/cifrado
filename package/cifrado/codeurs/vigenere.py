# -*- coding: utf-8 -*-

# Cifrado - Chiffrer et imprimer les messages de grand-jeu
# Copyright © 2006-2007 Étienne Bersac — Tous droits réservés.

import  gtk
from    ..      import    codeur
from    curses  import  ascii

import  gettext
_ = gettext.gettext

def charger(node):
    codeur = Codeur()
    codeur.clef = node.getAttribute('clef')
    return codeur

class Codeur(codeur.Codeur):
    nom = _('Vigenere')

    def __init__(self, dest=None):
        if dest and dest.clef:
            self.clef = dest.clef
        else:
            # traducteur: clef par défaut pour le vigenere
            self.clef = _('clef')
        
    def convertir(self, text):
        """Décale dynamiquement l'alphabêt suivant une clef donné par l'utilisateur"""
        text = self.strip_special_chars(text)
        result = ''
        clef = self.toalpha(self.clef.lower())

        i = 0
        for char in text:
            if (char.isalpha()):
                casse = self.casse (char)
                index = (self.letters[casse].index(char) + self.letters[self.casse(clef[i])].index(clef[i])) % 26
                result+= self.letters[casse][index]
                i = (i + 1) % len(clef)
            else:
                result+= char

        return result

    def get_prop_widget(self):
        box = gtk.HBox(False, 6)
        l = gtk.Label()
        # traduction: étiquette du champ de configuration du vigenere
        l.set_markup('<b>'+_('Clef :')+'</b>')
        box.pack_start(l, False, True, 0)
        entry = gtk.Entry()
        entry.set_text(self.clef)
        entry.connect('changed', self.on_entry_changed)
        box.pack_start(entry, True, True, 0)
        return box

    def get_prop_desc(self):
        return self.clef

    def sauver(self, doc, element):
        element.setAttribute('clef', self.clef)

    # CALLBACKS
    def on_entry_changed(self, entry):
        self.clef = entry.get_text()
        self.changed()

