# -*- coding: utf-8 -*-

# Cifrado - Chiffrer et imprimer les messages de grand-jeu
# Copyright © 2006-2007 Étienne Bersac — Tous droits réservés.

from    ..      import    codeur

import  gettext
_ = gettext.gettext

def     charger(node):
    return Codeur()

class Codeur(codeur.Codeur):
    # traducteur: nom du codeur
    nom = _('Aucun')

    def get_interligne(self):
        return 0
