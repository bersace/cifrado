# -*- coding: utf-8 -*-

# Cifrado - Chiffrer et imprimer les messages de grand-jeu
# Copyright © 2008 Étienne Bersac — Tous droits réservés.

import  gtk
from    ..      import    codeur

import  gettext
_ = gettext.gettext

def charger(node):
    return Codeur()
    
class Codeur(codeur.Codeur):
    #traducteurs: codage décrit en espagnol à http://www.gentedelsur.cl/scout/agujerito.htm
    nom = _("Cenit Polar")
    table = {'C':'P','c':'p',
             'E':'O','e':'o',
             'N':'L','n':'l',
             'I':'A','i':'a',
             'T':'R','t':'r',
             'P':'C','p':'c',
             'O':'E','o':'e',
             'L':'N','l':'n',
             'A':'I','a':'i',
             'R':'T','r':'t'}

    def convertir(self, text):
        text = self.strip_special_chars(text)
        result = "";
        
        for char in text:
            if char.isalpha() and Codeur.table.has_key(char):
                result+= Codeur.table[char]
            else:
                result+= char
            
        return result
