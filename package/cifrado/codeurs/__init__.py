# -*- coding: utf-8 -*-

import  gettext
_ = gettext.gettext

# traducteurs : ceci est la liste des codeurs présentés à
# l'utilisateur. Par défaut, tout les codeurs sont listés. Cependant,
# certains sont trop spécifiques à une langue voire une région. Pour
# supprimer un codeur, copier la liste complète et supprimer un nom de
# codeur. Attention à ne pas trop en supprimer. Veillez à garder
# uniquement des noms de codeurs existant et des virgules uniquement.
codeurs = _("aucun,braille,caesar,chiffrer,agujerito,murcielago,"
            "inverser,morpion,morse,"
            "roulement,polybius,cenitpolar,samourai,templier,vigenere")

__all__ = codeurs.split(",")
