# -*- coding: utf-8 -*-

# Cifrado - Chiffrer et imprimer les messages de grand-jeu
# Copyright © 2006-2007 Étienne Bersac — Tous droits réservés.

import  gtk
from    ..      import    codeur

import  gettext
_ = gettext.gettext

def charger(node):
    codeur = Codeur()
    return codeur
    
class Codeur(codeur.Codeur):
    # traducteur: nom du codeur
    nom = _('Cæsar')


    def __init__(self):
        self.decalage = 3

    def convertir(self, text):
        """Décale de trois lettres et chiffres."""
        text = self.strip_special_chars(text)
        result = "";
        
        for char in text:
            if char.isalpha():
                if (char.islower()):
                    liste = self.letters["lower"]
                else:
                    liste = self.letters["upper"]
                    
                result+=liste[(liste.index(char) + self.decalage) % 26]
            elif char.isdigit():
                result+=str((int(char)+self.decalage)%10)
            else:
                result+= char
            
        return result

