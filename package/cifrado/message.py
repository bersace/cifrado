# -*- coding: utf-8 -*-

# Cifrado - Chiffrer et imprimer les messages de grand-jeu
# Copyright © 2006-2007 Étienne Bersac — Tous droits réservés.

import  gobject
import  gtk
import  re
import  sys

import  gettext
_ = gettext.gettext

import  jeu
from    .       import  emission
from    .       import  codeurs
from    .       import  module
from    .       import  imprimeur
from    .       import  defs

def charger(el, jeu):
    titre = el.getAttribute('titre')
    date = el.getAttribute('date')
    exp = jeu.forces[el.getAttribute('exp').encode('utf-8')]

    try:
        message = el.getElementsByTagName('texte')[0].childNodes[0].nodeValue.strip()
    except IndexError:
        message = ""

    emissions = {}
    for e in el.getElementsByTagName('emission'):
        f = e.getAttribute('dest').encode('utf-8')
        if jeu.forces.has_key(f) and jeu.forces[f]:
            em = emission.charger(e, jeu)
            emissions[em.dest] = em

    msg = Message(exp, titre, date, message, jeu, emissions)

    return msg

class Module(module.Module):
    def _init(self, cifrado):
        # messages
        self.messages   = gtk.ListStore(gobject.TYPE_PYOBJECT,
                                        gobject.TYPE_STRING,
                                        gobject.TYPE_STRING,
                                        gobject.TYPE_STRING)

        liste = self.ui.get_object('msg_liste')
        liste.set_model(self.messages)

        self.ui.get_object('msg_liste').get_selection().connect('changed',
                                                                   self.on_msg_selected)
        self.ui.get_object('msg_champ').get_buffer().connect('changed',
                                                                self.on_msg_changed)
        self.spellcheck(['msg_champ'])

        # exp
        renderer = gtk.CellRendererCombo()
        renderer.set_property('editable', True)
        renderer.set_property('has-entry', False)
        renderer.set_property('text-column', defs.COL_FOR_NOM)
        self.msg_exp_renderer = renderer
        renderer.connect('edited', self.on_msg_exp_edited)
        # traducteurs: titre de colonne
        colonne = gtk.TreeViewColumn(_('Expéditeur'), renderer,
                                     text=defs.COL_MSG_EXP)
        liste.append_column(colonne)

        # titre
        renderer = gtk.CellRendererText()
        renderer.set_property('editable', True)
        renderer.connect('edited', self.on_msg_titre_edited)
        # traducteurs: titre de colonne
        colonne = gtk.TreeViewColumn(_('Titre'), renderer,
                                     text=defs.COL_MSG_TITRE)
        colonne.set_expand(True)
        liste.append_column(colonne)

        # date
        renderer = gtk.CellRendererText()
        renderer.set_property('editable', True)
        renderer.connect('edited', self.on_msg_date_edited)
        # traducteurs: titre de colonne
        colonne = gtk.TreeViewColumn(_('Date'), renderer,
                                     text=defs.COL_MSG_DATE)
        colonne.set_expand(False)
        liste.append_column(colonne)

        cifrado.modules['force'].forces.connect('row-changed', self.forces_row_changed)
        cifrado.modules['force'].forces.connect('row-deleted', self.forces_row_deleted)

    def _ouvrir(self, jeu):
        self.msg_exp_renderer.set_property('model',
                                           self.cifrado.modules['force'].forces)

        for m in jeu.messages:
            self.ajouter_message(m)

        self.on_msg_selected(self.ui.get_object('msg_liste').get_selection())

    def _fermer(self):
        self.messages.clear()


    # gestion des messages
    def get_iter_message(self, msg):
        return self.get_iter(self.messages, defs.COL_MSG, msg)

    def ajouter_message(self, msg):
        self.messages.append([msg, msg.exp.nom, msg.titre, msg.date])

    def supprimer_message(self, message):
        iter = self.get_iter_message(message)
        self.jeu.messages.remove(message);
        self.messages.remove(iter)


    def get_msg_crt(self):
        liste = self.ui.get_object('msg_liste')
        sel = liste.get_selection()
        model, iter = sel.get_selected()
        if model is not None and iter is not None and model.iter_is_valid(iter):
            msg = model.get_value(iter, defs.COL_MSG)
        else:
            msg = None
        return msg


    # MESSAGES
    def on_msg_nv_btn_clicked(self, bouton):
        # choisir un expéditeur
        tree, iter = self.ui.get_object('forces_liste').get_selection().get_selected()
        if iter is None:
            iter = self.cifrado.modules['force'].forces.get_iter_first()
        exp = self.cifrado.modules['force'].forces.get_value(iter, defs.COL_FORCE)

        # créer les émissions
        msg = Message(exp=exp,
                      # traducteurs: titre par défaut d'un nouveau message
                      titre=_("Nouveau message"),
                      # traducteurs: date par défaut de dépôt d'un message.
                      date=_("jour/heure"),
                      message="",
                      jeu=self.jeu)
        self.ajouter_message(msg)
        self.jeu.messages.append(msg)

        iter = self.get_iter_message(msg)
        self.ui.get_object('msg_liste').get_selection().select_iter(iter)
        self.cifrado.sync(False)

    def on_msg_selected(self, selection):
        if self.jeu is None:
            return

        sync = self.jeu.sync;
        liste, iter = selection.get_selected()

        wids = ['msg_impr_btn', 'msg_suppr_btn', 'msg_onglets']
        for id in wids:
            self.ui.get_object(id).set_sensitive(iter is not None)

        if iter:
            msg = liste.get_value (iter, defs.COL_MSG)
            self.ui.get_object('msg_champ').get_buffer().set_text(msg.message)
            # sélectionner le premier destinataire
            ems = self.ui.get_object('em_dest_select')
            model = ems.get_model()
            iter = model.get_iter_first()
            while iter is not None and model.iter_is_valid(iter):
                dest = model.get_value(iter, defs.COL_FORCE)
                em = self.get_msg_crt().emissions[dest]
                if em.env is True:
                    break
                iter = model.iter_next(iter)

            # ou par défaut le premier
            if iter is None or not model.iter_is_valid(iter):
                iter = model.get_iter_first()

            ems.set_active_iter(iter)
            self.cifrado.modules['em'].on_em_dest_select_changed(ems)
            
        else:
            self.ui.get_object('msg_champ').get_buffer().set_text('')
        self.sync(sync)


    def on_msg_exp_edited(self, renderer, path, force):
        iter = self.messages.get_iter(path)
        self.messages.set(iter, defs.COL_MSG_EXP, force)
        msg = self.messages.get_value(iter, defs.COL_MSG)
        msg.exp = self.jeu.forces[force]
        self.cifrado.sync(False)

    def on_msg_titre_edited(self, renderer, path, titre):
        iter = self.messages.get_iter(path)
        msg = self.messages.get_value(iter, defs.COL_MSG)
        msg.titre = titre
        self.messages.set(iter, defs.COL_MSG_TITRE, titre)
        self.cifrado.sync(False)
        
    def on_msg_date_edited(self, renderer, path, date):
        iter = self.messages.get_iter(path)
        msg = self.messages.get_value(iter, defs.COL_MSG)
        msg.date = date
        self.messages.set(iter, defs.COL_MSG_DATE, date)
        self.cifrado.sync(False)
        
    def on_msg_suppr_btn_clicked(self, bouton):
        liste, iter = self.ui.get_object('msg_liste').get_selection().get_selected()
        if iter is not None:
            msg = self.get_msg_crt()
            self.jeu.messages.remove(msg)
            self.messages.remove(iter)
        self.sync(False)

    def on_msg_impr_btn_clicked(self, bouton):
        self.cifrado.imprimer(self.get_msg_crt())


    def on_msg_changed(self, tampon):
        msg = self.get_msg_crt()
        if msg is None:
            return
        msg.message = tampon.get_text(tampon.get_start_iter(),
                                      tampon.get_end_iter())

        self.cifrado.modules['em'].actualiser_substitutions()

        self.sync(False)


    def forces_row_changed(self, forces, path, iter):
        """À la modification/création d'un jeu, ajouter l'émission
        correspondante"""
        if self.jeu is None:
            return

        force = forces.get_value(iter, defs.COL_FORCE)

        lieu = self.jeu.lieux.values()[0]
        for m in self.jeu.messages:
            if force is not None and not m.emissions.has_key(force):
                m.emissions[force] = emission.Emission(False, force, lieu,
                                                       [codeurs.aucun.Codeur()],
                                                       msg=m)
            

    def forces_row_deleted(self, forces, path):
        """On boucle les forces restantes pour les garder. On supprime
        'les' autres."""

        if self.jeu is None:
            return

        for m in self.jeu.messages:
            for force in self.jeu.forces:
                garder = False

                for f in m.emissions:
                    garder = garder or (f is force)

                if not garder and m.emissions.has_key(force):
                    del m.emissions[force]



class Message(imprimeur.Imprimeur):
    def __init__(self, exp, titre, date, message, jeu, emissions=None):
        self.exp        = exp
        self.titre      = titre
        self.date       = date
        self.message    = message
        self.capture_vars = re.compile('(\$[A-Z_]{2,})')

        if emissions is None:
            self.emissions = {}
            l = jeu.lieux.values()[0]
            for f in jeu.forces.values():
                self.emissions[f] = emission.Emission(False, f, l)
        else:
            self.emissions = emissions

        for em in self.emissions.values():
            em.msg = self

    def lister_variables(self):
        variables = self.capture_vars.findall(self.message)
        # http://www.peterbe.com/plog/uniqifiers-benchmark
        seen = set()
        variables = [x for x in variables if x not in seen and not seen.add(x)]
        for var in defs.prevars:
            if var in variables:
                variables.remove(var)

        return variables

    # impression
    def init_print(self, jeu):
        self.print_op.set_job_name(_("Message « %s »")%(self.titre))

        self._j = 0
        self.pages = []
        self.jeu = jeu
        if not self.page_setup:
            self.page_setup = jeu.page_setup

        for em in self.emissions.values():
            em.jeu = self.jeu
            em.page_setup = self.page_setup

    def begin_print(self, op, contexte):
        # traducteurs: titre du travail d'impression d'un message. %s
        # est le titre du message.
        self.print_settings.set("output-uri", self.titre+".pdf")

    def paginate(self, op, contexte):
        for em in self.emissions.values():
            n = op.get_property('n-pages')
            if n < 0:
                n = 0

            em.paginate(op, contexte)

            nn = op.get_property('n-pages')
            if nn < 0:
                nn = 0

            nn-=max(0,n)
            if nn == 3:
                self.pages.append(None) # page vide
            if nn:
                self.pages.append(em)
            if nn > 1:
                self.pages.append(None) # page vide

        return True

    def draw_page(self, op, contexte, n):
        if self._j < len(self.pages):
            if self.pages[self._j]:
                self.pages[self._j].draw_page(op, contexte, n)
            self._j+=1


    def createXML(self, doc):
        m = doc.createElement('message')
        m.setAttribute('titre', self.titre)
        m.setAttribute('exp', self.exp.nom)
        m.setAttribute('date', self.date)
        
        e = doc.createElement('texte')
        m.appendChild(e)
        e.appendChild(doc.createTextNode(self.message))

        for f in self.emissions:
            m.appendChild(self.emissions[f].createXML(doc))

        return m
