# -*- coding: utf-8 -*-

# Cifrado - un modéliseur de messages de grand-jeu
# Copyright © 2006-2008 Étienne Bersac — Tous droits réservés.

import  gtkspell

class Module:
    def __init__(self, cifrado):
        self.cifrado    = cifrado
        self.ui         = cifrado.ui
        self.jeu        = None
        self._init(cifrado)

    def spellcheck(self, ids,lang=None):
        for id in ids:
            s = gtkspell.Spell(self.ui.get_object(id), lang)

    def _init(self, cifrado):
        pass

    def ouvrir(self, jeu):
        self.jeu = jeu
        self._ouvrir(jeu)

    def _ouvrir(self, jeu):
        pass

    def sauver(self, jeu):
        pass

    def fermer(self):
        self._fermer()
        self.jeu = None

    def _fermer(self):
        pass
    
    def get_iter(self, liste, col, obj):
        """Retourne l'itérateur correspondant à un objet obj à la
        colonne col de liste."""
        iter = liste.get_iter_first()
        while iter is not None and liste.iter_is_valid(iter):
            o = liste.get_value(iter, col)
            if o is obj:
                return iter
            iter = liste.iter_next(iter)

    def select_combo(self, combo, col, val):
        """Sélectionne la ligne de combo dont la colonne col contient val"""
        model = combo.get_model()
        i = model.get_iter_first()
        while i is not None:
            cell = model.get_value(i, col)
            if cell == val:
                combo.set_active_iter(i)
                break
            i = model.iter_next(i)

    def sync(self, synced=False):
        self.cifrado.sync(synced)
