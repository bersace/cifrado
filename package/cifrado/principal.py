# -*- coding: utf-8 -*-

# Cifrado - un modéliseur de messages de grand-jeu
# Copyright © 2006-2008 Étienne Bersac — Tous droits réservés.

import  time
import  os.path
import  gtk
import  gobject
import  pango
import  xdg.BaseDirectory       as xdgbd
import  time
from    xml.dom.minidom import  Document,parse

import  gettext
_ = gettext.gettext

from    .       import jeu
from    .       import etape
from    .       import message
from    .       import emission
from    .       import force
from    .       import lieu
from    .       import codeurs
from    .       import carte
from    .       import defs

import  urllib
from    urllib  import urlopen


# Cette classe gère :
# * Vision globale du jeu courant (creation, impression, sauvegarde)
# * l'interface
class Principal:
    """Classe principale de Cifrado, gérant l'interface graphique"""

    def __init__(self):

        gtk.window_set_default_icon_name('cifrado')
        self.ui = gtk.Builder()
        self.ui.add_from_file(defs.__ui__)

        self.wids = ['cifrado_onglets', 'imprimer_btn', 'enregistrer_btn',
                     'menu_jeu_fermer', 'menu_jeu_enr', 'menu_jeu_enr_sous', 'menu_jeu_imprimer']

        dic = {
            # quitter
            'on_cifrado_fenetre_delete_event':          self.on_cifrado_fenetre_delete_event,
            'on_quitter_btn_clicked':                   self.on_quitter_btn_clicked_event,
            'on_menu_jeu_quitter_activate':             self.on_menu_jeu_quitter_activate,
            'on_menu_jeu_fermer_activate':              self.on_menu_jeu_fermer_activate,
            # ouvrir
            'on_menu_jeu_nv_activate':                  self.on_menu_jeu_nv_activate,
            'on_menu_jeu_ouv_activate':                 self.on_menu_jeu_ouv_activate,
            # enregistrer
            'on_enregistrer_btn_clicked':               self.on_enregistrer_btn_clicked,
            'on_menu_jeu_enr_activate':                 self.on_menu_jeu_enr_activate,
            'on_menu_jeu_enr_sous_activate':            self.on_menu_jeu_enr_sous_activate,
            # aide
            'on_menu_aide_apropos_activate':            self.on_menu_aide_apropos_activate,
            'on_apropos_response':                      self.on_apropos_response,
            'on_menu_aide_aide_activate':               self.on_menu_aide_aide_activate,
            # impression
            'on_menu_jeu_page_prop_activate':           self.on_menu_jeu_page_prop_activate,
            # expander
            'on_trame_exp_activate':                    self.on_champ_exp_activate,
            'on_scenario_exp_activate':                 self.on_champ_exp_activate,
            }

        # MODULES
        #
        # Les modules étendent les fonctionnalités de l'interface, de
        # l'impression, etc. Le cœur du programme va les appeler
        # explicitement.
        #
        self.modules = {}
        self.modules['jeu']     = jeu.Module(self)
        self.modules['etape']   = etape.Module(self)
        self.modules['force']   = force.Module(self)
        self.modules['lieu']    = lieu.Module(self)
        self.modules['message'] = message.Module(self)
        self.modules['em']      = emission.Module(self)
        self.modules['carte']   = carte.Module(self)

        for module in self.modules.values():
            for attrname in dir(module):
                if not attrname.startswith('on_'):
                    continue
                attr = getattr(module, attrname)
                dic[attrname] = attr

        self.ui.connect_signals(dic)

        
        # CHARGEMENT
        
        f = gtk.FileFilter()
        f.add_mime_type('application/x-cifrado+xml')
        f.set_name(_('Grand-jeu cifrado'))
        
        d = self.ui.get_object('select_fichier_ouv')
        d.add_buttons(gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL,
                      gtk.STOCK_OPEN, gtk.RESPONSE_OK)
        d.add_filter(f)
                      
                      
        # SAUVEGARDE    
                      
        d = self.ui.get_object('select_fichier_enr')
        d.add_buttons(gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL,
                      gtk.STOCK_SAVE, gtk.RESPONSE_OK)
        d.add_filter(f)

        # IMPRESSION
        self.page_setup = gtk.PageSetup()
        # hack dégueux :
        #self.page_setup.set_bottom_margin(self.page_setup.get_top_margin(gtk.UNIT_POINTS), gtk.UNIT_POINTS)
        self.print_settings = gtk.PrintSettings()

        # À PROPOS
        d = self.ui.get_object('apropos')
        d.set_name(defs.__package__)
        d.set_version(defs.__version__)
        d.set_comments(defs.__description__)
        d.set_website_label(_('Site web de Cifrado'))
        d.set_website(defs.__website__)

        self.jeu = None
        self.fichier = None

    def lancer(self):
        self.ui.get_object('cifrado_fenetre').show_all()
        gtk.main()

    def message(self, parent=None, titre="Attention !", message="",
                buttons=gtk.BUTTONS_CLOSE,
                type=gtk.MESSAGE_WARNING):
        if parent is None:
            parent = self.ui.get_object('cifrado_fenetre')
        dialog = gtk.MessageDialog(parent,
                                   gtk.DIALOG_MODAL
                                   & gtk.DIALOG_DESTROY_WITH_PARENT,
                                   type,
                                   buttons)
        dialog.set_markup("<big><b>%s</b></big>"%(titre))
        dialog.set_title(titre)
        dialog.format_secondary_markup(message)
        res = dialog.run()
        dialog.hide()
        return res
                                   

    def imprimer(self,imprimeur):

        op = gtk.PrintOperation()
        op.set_print_settings(self.print_settings) # est-ce que ça fonctionne ?
        op.set_default_page_setup(self.page_setup)

        self.jeu.numeroter_msg()
        if imprimeur is None:
            imprimeur = self.jeu

        imprimeur.jeu           = self.jeu
        imprimeur.print_op      = op
        imprimeur.print_settings= self.print_settings
        imprimeur.page_setup    = self.page_setup

        op.titre_pages = []
        op.connect('begin-print',       imprimeur.begin_print)
        op.connect('paginate',          imprimeur.paginate)
        op.connect('draw-page',         imprimeur.draw_page)
        op.connect('draw-page',         self.draw_page)
        op.connect('status-changed',    self.print_status_changed)
        op.connect('end-print',         self.end_print)

        imprimeur.init_print(self.jeu)

        op.run(gtk.PRINT_OPERATION_ACTION_PRINT_DIALOG,
               self.ui.get_object('cifrado_fenetre'))

        self.print_settings = op.get_print_settings()


    def end_print(self,op,contexte):
        self.sync(False)

    def quitter(self):
        self.fermer()
        gtk.main_quit()
        

    def charger(self, fichier):
        if fichier is None:
            d = self.ui.get_object('select_fichier_ouv')
            d.set_current_folder(xdgbd._home)
            r = d.run()
            d.hide()
            if r != gtk.RESPONSE_OK.__index__():
                return
            else:
                fichier = d.get_filename()

        self.fermer()
        self.fichier = fichier
        flux = urlopen(fichier)
        doc = parse(flux)
        flux.close()
        el = doc.childNodes[0]
        j = jeu.charger(el, fichier)
        self.ouvrir(j)
        self.charger_options_impression(el)
        self.register_recent()

    def charger_options_impression(self, doc):
        el = doc.getElementsByTagName('impression')
        if len(el) < 1:
            return
        else:
            el = el[0]

        # charger la taille
        s = el.getAttribute('taille')
        if s:
            ps = gtk.PaperSize(s)
            self.page_setup.set_paper_size(ps)

        # charger l'orientation
        o = el.getAttribute('orientation')
        if o:
            po = gtk.PageOrientation.__enum_values__[int(o)]
            self.page_setup.set_orientation(po)

        # charger les options d'impression
        for e in el.getElementsByTagName('option'):
            clef, valeur = e.getAttribute('clef'), e.getAttribute('valeur')
            self.print_settings.set(clef, valeur)

    def sauver_option_impression(self, clef, val, doc):
        """Sauvegarder une option d'impression"""
        doc, impr = doc
        el = doc.createElement('option')
        el.setAttribute('clef', clef)
        el.setAttribute('valeur', val)
        impr.appendChild(el)

    def sauver_options_impression(self, doc):
        el = doc.createElement('impression')

        # sauver la taille
        ps = self.page_setup.get_paper_size()
        el.setAttribute('taille', ps.get_name())

        # sauver l'orientation
        po = self.page_setup.get_orientation()
        el.setAttribute('orientation', str(po.__index__()))

        # sauver les options
        self.print_settings.foreach(self.sauver_option_impression, (doc, el))

        doc.childNodes[0].appendChild(el)

    def sauver(self, fichier=None):
        if fichier is None and self.fichier is None:
            d = self.ui.get_object('select_fichier_enr')
            d.set_current_folder(xdgbd._home)
            d.set_current_name("%s.cifrado"%(self.jeu.nom))
            r = d.run()
            d.hide()
            if r != gtk.RESPONSE_OK.__index__():
                return
            else:
                fichier = self.fichier = d.get_filename()
        else:
            fichier = self.fichier

        for m in self.modules:
            self.modules[m].sauver(fichier)

        doc = Document()
        doc.appendChild(self.jeu.createXML(doc))
        self.sauver_options_impression(doc)
        flux = open(urllib.unquote(fichier.replace("file://","")), "w")
        flux.write(doc.toprettyxml())
        flux.close()
        self.register_recent()
        print _("Jeu %(nom)s sauvé dans %(fichier)s")%{'nom':self.jeu.nom,'fichier':self.fichier}
        self.sync(True)

    def sync(self, sync):
        self.jeu.sync = sync
        wids = ['enregistrer_btn', 'menu_jeu_enr']
        for wid in wids:
            self.ui.get_object(wid).set_sensitive(not self.jeu.sync)

    def register_recent(self):
        rec = gtk.recent_manager_get_default()
        if rec.has_item(self.fichier):
            rec.remove_item(self.fichier)

        titre = str(_("Grand-jeu « %s »")%(self.jeu.nom))
        recent_data = {"mime_type": "application/x-cifrado+xml",
                       "app_name": "Cifrado",
                       "app_exec": "cifrado %u",
                       "display_name": titre,
                       "description": titre,
                       "is_private": False,
                       "groups" : ['cifrado']}
        rec.add_full(self.fichier, recent_data)
        print _("%s enregistré dans les documents récents")%self.fichier


    def nouveau(self):
        self.fermer()
        nj = jeu.Jeu(nom=_('Nouveau Jeu'),
                     # traducteurs: trame imaginaire par défaut, sera
                     # affichée dans le champ d'édition de la trame.
                     trame=_("Raconter ici votre trame imaginaire"),
                     nb_chefs=4)
        self.fichier = None

        l = lieu.Lieu(_('Nouveau lieu'))
        nj.lieux[l.nom] = l

        f = force.Force(_('Nouvelle force'))
        nj.forces[f.nom] = f

        msg = message.Message(exp=f,
                              titre=_("Nouveau message"),
                              date=_("jour/heure"),
                              message="",
                              jeu=nj)
        nj.messages.append(msg)
        self.ouvrir(nj)
        self.sync(True)


    def fermer(self):
        if not self.jeu:
            return
        if not self.jeu.sync:
            res = self.message(buttons=gtk.BUTTONS_YES_NO,
                               titre = _("Voulez-vous sauvegarder ?"),
                               message = _("Le jeu en édition n'est pas sauvegardé. Si vous le fermer maintenant, toute vos modifications seront perdues !"))
                         
            if res == gtk.RESPONSE_YES.__index__():
                self.sauver()

        for m in self.modules:
            self.modules[m].fermer()

        for id in self.wids:
            self.ui.get_object(id).set_sensitive(False)

        # fenêtre
        self.ui.get_object('cifrado_fenetre').set_title('Cifrado')

        self.jeu = None
        self.fichier = None
        

    def ouvrir(self, jeu):
        self.jeu = jeu

        for k in self.modules:
            self.modules[k].ouvrir(jeu)

        for id in self.wids:
            self.ui.get_object(id).set_sensitive(True)

        self.sync(True)



    #####################
    #   gestionnaires   #
    #####################

    def on_champ_exp_activate(self, widget):
        container = widget.get_parent()
        val = not widget.get_property('expanded')
        if isinstance(container, gtk.Box):
            prop = 'expands'
        elif isinstance(container, gtk.Paned):
            prop = 'resize'

        container.child_set_property(widget, prop, val)

    def print_status_changed(self, op):
        status = op.get_status()
        d = self.ui.get_object('suivi_impression')
        p = self.ui.get_object('progression_impression')
        e = self.ui.get_object('etat_impression')

        if status == gtk.PRINT_STATUS_GENERATING_DATA:
            p.set_fraction(.05)
            # traducteurs: état de l'impression
            p.set_text(_("Initialisation"))
            d.show_all()
        elif status == gtk.PRINT_STATUS_SENDING_DATA:
            p.set_fraction(1.)
            # traducteurs: état de l'impression
            p.set_text(_("Pages générée"))
            # traducteurs: état de l'impression
            e.set_markup("<i>%s</i>"%_("Envoi des pages à l'imprimante"))
        elif status == gtk.PRINT_STATUS_FINISHED:
            d.hide()
            

    def draw_page(self, op, contexte, n):
        if n==-1 or n > len(op.titre_pages):
            return
        t = op.get_property('n-pages')
        e = self.ui.get_object('etat_impression')
        # traducteurs: état de l'impression, %s est le titre de la
        # page en cour d'impression
        e.set_markup("<i>%s</i>"%_("Impression de la page %s")%(op.titre_pages[n]))

        p = self.ui.get_object('progression_impression')
        # traducteurs: N° de la page en cours d'impression.
        p.set_text(_("Page %(page)i de %(total)i")%{'page':n+1,'total':t})
        p.set_fraction((n+1.)/float(t))
        
    # NOUVEAU
    def on_menu_jeu_nv_activate(self, item):
        self.nouveau()

    # CHARGER
    def on_menu_jeu_ouv_activate(self, item):
        self.charger(None)

    # SAUVER
    def on_enregistrer_btn_clicked(self, bouton):
        self.sauver(self.fichier)

    def on_menu_jeu_enr_activate(self, item):
        self.sauver(self.fichier)

    def on_menu_jeu_enr_sous_activate(self, item):
        self.sauver(None)





    # IMPRIMER
    def on_menu_jeu_page_prop_activate(self, item):
        self.page_setup = gtk.print_run_page_setup_dialog(self.ui.get_object('cifrado_fenetre'),
                                                          self.page_setup,
                                                          self.print_settings)
        self.sync(False)

    # FERMER
    def on_menu_jeu_fermer_activate(self, item):
        self.fermer()

    # QUITTER
    def on_cifrado_fenetre_delete_event(self, fenetre, event):
        gobject.idle_add(self.quitter)

    def on_menu_jeu_quitter_activate(self, item):
        gobject.idle_add(self.quitter)

    def on_quitter_btn_clicked_event(self, bouton):
        gobject.idle_add(self.quitter)

    # À PROPOS
    def on_menu_aide_apropos_activate(self, item):
        apropos = self.ui.get_object('apropos')
        apropos.run()
    
    def on_apropos_response(self, apropos, reponse):
        apropos.hide()

    def on_menu_aide_aide_activate(self, item):
        gtk.show_uri(self.ui.get_object('cifrado_fenetre').get_screen(),
                     'ghelp:cifrado',
                     gtk.get_current_event_time())

