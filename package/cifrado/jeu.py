# -*- coding: utf-8 -*-

# Cifrado - Chiffrer et imprimer les messages de grand-jeu
# Copyright © 2006-2008 Étienne Bersac — Tous droits réservés.

import  gobject
import  gtk
import  pango
import  sys,os
import  traceback

import  gettext
_ = gettext.gettext

from    .       import  force
from    .       import  etape
from    .       import  lieu
from    .       import  message
from    .       import  module
from    .       import  imprimeur
imp = imprimeur
from    .       import  defs
from    .       import  carte

def charger(el, fichier):
    jeu = Jeu(fichier=fichier,
              nom = el.getAttribute('nom'),
              nb_chefs = int(el.getAttribute('nb-chefs')),
              trame = el.getElementsByTagName('trame')[0].childNodes[0].nodeValue.strip())

    for e in el.getElementsByTagName('lieu'):
        l = lieu.charger(e)
        jeu.lieux[l.nom] = l

    for e in el.getElementsByTagName('force'):
        f = force.charger(e)
        jeu.forces[f.nom] = f

    for e in el.getElementsByTagName('etape'):
        t = etape.charger(e)
        jeu.etapes.append(t)

    for e in el.getElementsByTagName('message'):
        jeu.messages.append(message.charger(e, jeu))

    for e in el.getElementsByTagName('carte'):
        jeu.carte = carte.charger(e, jeu)

    jeu.sync = True

    return jeu

class Module(module.Module):
    """Gère l'onglet 'Jeu'."""

    def _init(self, cifrado):
        self.ui.get_object('jeu_trame_champ').get_buffer().connect('changed',
                                                                   self.on_trame_changed)

        self.spellcheck(['jeu_trame_champ'])

    # Interface Module
    def _ouvrir(self, jeu):
        self.ui.get_object('jeu_nom_champ').set_text(jeu.nom)
        self.ui.get_object('jeu_trame_champ').get_buffer().set_text(jeu.trame)
        self.ui.get_object('jeu_nb_chefs_champ').set_value(float(jeu.nb_chefs))

    def _fermer(self):
        self.ui.get_object('jeu_nom_champ').set_text('')
        self.ui.get_object('jeu_nb_chefs_champ').set_value(0.)
        self.sync(True)

    # CALLBACKS
    def on_jeu_nom_champ_changed(self, champ):
        self.jeu.nom = champ.get_text()
        self.ui.get_object('cifrado_fenetre').set_title(self.cifrado.jeu.nom)
        self.sync(False)

    def on_jeu_nb_chefs_champ_value_changed(self, bouton):
        self.jeu.nb_chefs= bouton.get_value_as_int()
        self.sync(False)

    def on_menu_jeu_imprimer_activate(self, item):
        self.cifrado.imprimer(self.jeu)

    def on_imprimer_btn_clicked(self, bouton):
        self.cifrado.imprimer(self.jeu)



    def on_trame_changed(self, tampon):
        if self.jeu is None:
            return

        self.jeu.trame = tampon.get_text(tampon.get_start_iter(),
                                         tampon.get_end_iter())
        self.sync(False)




class Jeu(imprimeur.Imprimeur):
    def __init__(self, nom="",trame="", nb_chefs=4,fichier=None):
        self.nom        = nom
        self.trame      = trame
        self.nb_chefs   = nb_chefs
        self.sync       = True
        self.lieux      = {}
        self.forces     = {}
        self.etapes     = []
        self.messages   = []
        self.carte      = None
        if fichier:
            self.dossier, nom = os.path.split(fichier.replace("file://",""))

    # imprimer
    def numeroter_msg(self):
        n = 1
        for m in self.messages:
            for f in m.emissions:
                e=m.emissions[f]
                if e.env:
                    m.emissions[f].no = n
                    n+=1

    def layout_pages_chef(self, op, contexte):
        pages = []
        page = []

        pg = contexte.create_pango_context()
        # interligne en point cairo
        il = 6
        # TITRE
        layout = self.layout_text(contexte, self.nom,
                                  font='serif bold 16',
                                  align=pango.ALIGN_CENTER)
        page.append([0,0,layout])

        w,h = imp.csize(layout)
        th = h+il+il

        # FORCES EN PRÉSENCE

        # traducteurs: titre de la liste des forces dans les pages de
        # chefs
        layout = self.layout_text(contexte, _("Forces en présence"),
                                  font='serif bold 11')
        page.append([0,th,layout])
        w,h = imp.csize(layout)
        sw = w
        sth = th
        sth+=h+il

        liste = []
        for force in self.forces.values():
            if len(force.acteur):
                item = "%s (%s)"%(force.nom,force.acteur)
            else:
                item = force.nom
            liste.append(item)
        layouts = self.layout_liste(contexte, liste)
        page.append([il,sth,layouts])
        w,h = imp.clsize(layouts)
        sth+=h
        sw = max(sw, il+w)

        # TRAME
        tth = 0
        if self.trame:
            x=sw+il+il
            # traducteurs: titre de la trame imaginaire dans les pages
            # de chefs
            layout = self.layout_text(contexte, _("Imaginaire"),
                                      font='serif bold 11',
                                      width=-x)
            w,h=imp.csize(layout)
            page.append([x,th,layout])
            tth = th
            tth+=h+il

            layout = self.layout_text(contexte, self.trame,width=-x,justify=True)
            page.append([x+il,tth,layout])
            w,h = imp.csize(layout)
            tth+=h
        th = max(tth,sth)+il*1.5

        # SCÉNARIO
        if len(self.etapes) == 0:
            return [page]

        layout = self.layout_text(contexte, _("Scénario"),font='serif bold 11')
        page.append([0,th,layout])
        w,h = imp.csize(layout)
        th+=h+il
        mw = 0
        for i in range(len(self.etapes)):
            etape = self.etapes[i]
            le = []
            eh = th

            # titre
            layout = self.layout_text(contexte,etape.titre,font='serif bold 9')
            w,h = imp.csize(layout)
            le.append([il,th,layout])

            layout = self.layout_text(contexte,
                                      "%s %s"%(etape.date,etape.lieu),
                                      font='serif italic 8')
            le.append([il+w+il,th+1,layout])
            w,h = imp.csize(layout)
            mw = max(mw,w)
            th+=h+il/3

            x = il+il
            w=contexte.get_width()-x
            r=6
            # items
            if len(etape.items):
                # traducteurs: titre de la liste des items à apporter
                # pour une étape du scénario.
                items = self.layout_liste(contexte,etape.items,width=w/r)
                iw, ih = imp.clsize(items)
                le.append([x,th,items])
            else:
                iw = ih = 0

            if iw:
                x+=w/r+il
            # préparation
            sth=th
            if etape.preparation:
                layout = self.layout_text(contexte,etape.preparation,justify=True,width=-x)
                pw,ph = imp.csize(layout)
                le.append([x,th,layout])
                ph+=il
                sth+=ph
            else:
                pw = ph = 0

            # activité
            if etape.activite:
                layout = self.layout_text(contexte,etape.activite,justify=True,width=-x)
                le.append([x,sth,layout])
                aw,ah = imp.csize(layout)
            else:
                aw = ah = 0

            # hauteur inter-titre.
            h = max(ih,ph+ah)

            # interligne si contenu supplémentaire
            if h:
                th+=h
            th+=il
            # position verticale de l'étape = position actuelle - hauteur de l'étape.
            eh = th-eh

            if th > contexte.get_height():
                pages.append(page)
                page = []
                print "nouvelle page", etape.titre
                for l in le:
                    l[1]-=th-eh
                th = eh
            page.extend(le)

        pages.append(page)
        return pages

    def init_print(self, jeu):
        for m in self.messages:
            m.print_op = self.print_op
            m.page_setup = self.page_setup
            m.init_print(self)

        op = self.print_op
        op.set_job_name(_("Grand-Jeu « %s »")%(self.jeu.nom))
        ps = op.get_print_settings()
        ps.set("output-uri", self.jeu.nom+".pdf")

    def begin_print(self, op, contexte):

        self.pages = []
        ps = op.get_print_settings()
        duplex = ps.get_duplex() != gtk.PRINT_DUPLEX_SIMPLEX

        n = 0
        try:
            self.pages_chef = self.layout_pages_chef(op, contexte)
        except Exception, e:
            print e
            traceback.print_tb(sys.exc_info()[2])
            print e
            sys.exit()
        npc = len(self.pages_chef) # nombre de page de chef
        nppc = npc              # nombre de pages par chef
        cartes = self.carte is not None and not self.carte.vide()
        if cartes:
            nppc+=1
            if duplex:
                # garder le total de page de chef pair
                nppc+=nppc%2

        n+= self.nb_chefs*nppc

        if n:
            op.set_n_pages(n)

        for i in range(n):
            li = i%nppc
            nc = i/nppc+1
            if li < npc:        # le npc premières pages
                self.pages.append(self)
                titre = _("Page chef N°%i")%(nc)
            elif cartes and li == npc: # la page suivante
                self.pages.append(self.carte)
                titre = _("Carte chef N°%i")%(nc)
            else:               # la page de post-garde
                self.pages.append(None)
                titre = _("Carte chef N°%i")%(nc)
            op.titre_pages.append(titre)

        self.nppc = nppc
        self.sentinelle = n

        # messages
        for msg in self.messages:
            n =op.get_property('n-pages')
            msg.paginate(op, contexte)
            nn = op.get_property('n-pages')
            for i in range(nn - max(0,n)):
                self.pages.append(msg)
            n = nn

        n =op.get_property('n-pages')

        # cartes
        if cartes:
            self.carte.init_print(self)
            self.carte.paginate(op, contexte)
            nn = op.get_property('n-pages')
            nn-=max(0,n)
            for i in range(nn):
                if duplex and i%2:
                    self.pages.append(None)
                else:
                    self.pages.append(self.carte)

        # suppression d'une page blanche finale
        n = op.get_property('n-pages')
        li = len(self.pages)-1
        if self.pages[li] is None:
            n-=1
            del self.pages[li]
            op.set_n_pages(n)

    def paginate(self, op, contexte):
        return True

    def draw_page(self, op, contexte, n):
        if n==-1 or n > len(self.pages):
            return

        if not self.pages[n]:
            # certainement une page blanche intercallaire
            return

        imprimeur = self.pages[n]
        if imprimeur is self:
            self.impr_page_chef(op, contexte, n)
        elif imprimeur is self.carte and n < self.sentinelle:
            imprimeur.draw_page(op, contexte, n, True)
        elif imprimeur:
            imprimeur.draw_page(op, contexte, n)


    def impr_page_chef(self, op, contexte, no):
        cr = contexte.get_cairo_context()
        page = self.pages_chef[no%self.nppc]
        for item in page:
            x,y,l = item
            if not isinstance(l,list):
                ls = [(0,l)]
            else:
                ls = l

            for l in ls:
                self.impr_layout(contexte,l[1],x=x,y=y+l[0])

    # sauvegarde
    def createXML(self, doc):
        j = doc.createElement('jeu')
        j.setAttribute('nom', self.nom)
        j.setAttribute('nb-chefs', str(self.nb_chefs))

        e = doc.createElement('trame');
        j.appendChild(e);
        e.appendChild(doc.createTextNode(self.trame))

        for f in self.forces.values():
            j.appendChild(f.createXML(doc))

        for l in self.lieux.values():
            j.appendChild(l.createXML(doc))

        for m in self.messages:
            j.appendChild(m.createXML(doc))

        for et in self.etapes:
            j.appendChild(et.createXML(doc))

        if self.carte:
            j.appendChild(self.carte.createXML(doc))

        return j

    def __str__(self):
        return "<Jeu %s 0x%x>"%(self.titre,id(self))
