# -*- coding: utf-8 -*-

# Cifrado - Chiffrer et imprimer les messages de grand-jeu
# Copyright © 2006-2007 Étienne Bersac — Tous droits réservés.

import  string

def charger(node):
    module = node.getAttribute('module')
    mod = __import__(module, globals(), locals(), module, 0)
    codeur = mod.charger(node)
    return codeur

class   Codeur:
    nom = 'Sans nom'
    changed_func = None

    def __init__(self, force=None):
        pass

    # Coder API
    def get_prop_widget(self):
        return None

    def get_prop_desc(self):
        return None

    def convertir(self, text):
        return text

    def get_police(self):
        return None

    def get_interligne(self):
        return 1.5

    def createXML(self, doc):
        element = doc.createElement('codeur')
        element.setAttribute('module', self.__module__)
        self.sauver(doc, element)
        return element
    
    def sauver(self, doc, element):
        return None
    
    # Codeur helper

    letters = {'lower' : string.lowercase,
               'upper' : string.uppercase}
    
    def casse (self, char):
        if (char.isupper()):
            return 'upper'
        else:
            return 'lower'

    def strip_special_chars(self, text):
        table = {'á' : 'a', 'Á' : 'A',
                 'à' : 'a', 'À' : 'A',
                 'â' : 'a', 'Â' : 'A',
                 'ä' : 'a', 'Ä' : 'A',
                 'å' : 'a', 'Å' : 'A',
                 'é' : 'e', 'É' : 'E',
                 'è' : 'e', 'È' : 'E',
                 'ê' : 'e', 'Ê' : 'E',
                 'ë' : 'e', 'Ë' : 'E',
                 'ì' : 'i', 'Ì' : 'I',
                 'í' : 'i', 'Í' : 'I',
                 'î' : 'i', 'Î' : 'I',
                 'ï' : 'i', 'Ï' : 'I',
                 'ó' : 'o', 'Ó' : 'O',
                 'ô' : 'o', 'Ô' : 'O',
                 'ö' : 'o', 'Ö' : 'O',
                 'ø' : 'o', 'Ø' : 'O',
                 'ú' : 'u', 'Ú' : 'U',
                 'ù' : 'u', 'Ù' : 'U',
                 'û' : 'u', 'Û' : 'U',
                 'ü' : 'u', 'Ü' : 'U',
                 'ç' : 'c', 'Ç' : 'C',
                 'œ' : 'oe','Œ' : 'OE',
                 'æ' : 'ae','Æ' : 'AE'}

        for pair in table.iteritems():
            text = text.replace(pair[0], pair[1])
            
        return text

    def toalpha(self, text):
        text = self.strip_special_chars(text)
        for char in text:
            if not char.isalpha():
                text = text.replace (char, '')
                
        return text

    def changed(self):
        if self.changed_func:
            self.changed_func()

