# -*- coding: utf-8 -*-

# Cifrado - Chiffrer et imprimer les messages de grand-jeu
# Copyright © 2006-2008 Étienne Bersac — Tous droits réservés.

import  gobject
import  gtk

import  gettext
_ = gettext.gettext

from    .       import  module
from    .       import  defs

def charger(el):
    try:
        preparation = el.getElementsByTagName('preparation')[0].childNodes[0].nodeValue.strip()
    except IndexError:
        preparation = ""

    try:
        activite = el.getElementsByTagName('activite')[0].childNodes[0].nodeValue.strip()
    except IndexError:
        activite = ""

    etape = Etape(titre=el.getAttribute('titre'),
                  date=el.getAttribute('date'),
                  lieu=el.getAttribute('lieu'),
                  preparation=preparation,
                  activite=activite)

    for e in el.getElementsByTagName('item'):
        etape.items.append(e.getAttribute('item'))

    return etape
        

class Module(module.Module):
    # Module
    def _init(self, cifrado):

        self.etapes = gtk.ListStore(gobject.TYPE_PYOBJECT,
                                    gobject.TYPE_STRING,
                                    gobject.TYPE_STRING,
                                    gobject.TYPE_STRING)

        self.items = gtk.ListStore(gobject.TYPE_STRING)

        # VUES
        # ÉTAPES
        liste = self.ui.get_object('etapes_liste')
        liste.set_model(self.etapes)

        # titre
        renderer = gtk.CellRendererText()
        renderer.set_property('editable', True)
        renderer.connect('edited', self.on_etape_titre_edited)
        # traducteurs: titre de colonne
        colonne = gtk.TreeViewColumn(_('Titre'), renderer,
                                     text=defs.COL_ET_TITRE)
        colonne.set_expand(True)
        liste.append_column(colonne)

        # date
        renderer = gtk.CellRendererText()
        renderer.set_property('editable', True)
        renderer.connect('edited', self.on_etape_date_edited)
        # traducteurs: titre de colonne
        colonne = gtk.TreeViewColumn(_('Date'), renderer,
                                     text=defs.COL_ET_DATE)
        colonne.set_expand(False)
        liste.append_column(colonne)

        # lieu
        renderer = gtk.CellRendererText()
        renderer.set_property('editable', True)
        renderer.connect('edited', self.on_etape_lieu_edited)
        # traducteurs: titre de colonne
        colonne = gtk.TreeViewColumn(_('Lieu'), renderer,
                                     text=defs.COL_ET_LIEU)
        colonne.set_expand(False)
        liste.append_column(colonne)

        # ITEMS
        liste = self.ui.get_object('etape_items_liste')
        liste.set_model(self.items)

        # date
        renderer = gtk.CellRendererText()
        renderer.set_property('editable', True)
        renderer.connect('edited', self.on_item_edited)
        # traducteurs: titre de colonne
        colonne = gtk.TreeViewColumn(_('Item'), renderer,
                                     text=defs.COL_ITEM)
        colonne.set_expand(False)
        liste.append_column(colonne)


        self.ui.get_object('etape_dialog').add_buttons(gtk.STOCK_CLOSE, gtk.RESPONSE_CLOSE)


        # signaux
        self.ui.get_object('etapes_liste').get_selection().connect('changed',
                                                                      self.on_etape_selected)
        self.ui.get_object('etape_preparation_champ').get_buffer().connect('changed',
                                                                              self.on_preparation_changed)

        self.ui.get_object('etape_activite_champ').get_buffer().connect('changed',
                                                                           self.on_activite_changed)
        self.etapes.connect('row-deleted', self.on_etape_deleted)

        self.spellcheck(['etape_preparation_champ', 'etape_activite_champ'])




    def _ouvrir(self, jeu):
        for etape in jeu.etapes:
            self.ajouter_etape(etape)


    def _fermer(self):
        self.etapes.clear()


    # Méthodes
    def ajouter_etape(self, etape):
        self.etapes.append([etape, etape.date, etape.titre, etape.lieu])

    def get_etape_crt_iter(self):
        liste = self.ui.get_object('etapes_liste')
        sel = liste.get_selection()
        return sel.get_selected()

    def get_etape_crt(self):
        model, iter = self.get_etape_crt_iter()
        if model is not None and iter is not None and model.iter_is_valid(iter):
            et = model.get_value(iter, defs.COL_ETAPE)
        else:
            et = None
        return et
        

    # CALLBACKS
    def on_etape_nv_clicked(self, bouton):
        # traducteurs: nom par défaut d'une nouvelle étape.
        etape = Etape(titre=_('Étape %i')%(len(self.jeu.etapes)))
        self.ajouter_etape(etape)
        self.jeu.etapes.append(etape)
        self.sync(False)

    def on_etape_selected(self, selection):
        etape = self.get_etape_crt()
        self.items.clear()

        wids = ['etape_modifier', 'etape_suppr']
        sensitive = etape is not None
        for wid in wids:
            self.ui.get_object(wid).set_sensitive(sensitive)

        if etape is not None:
            self.ui.get_object('etape_dialog').set_title(etape.titre)
            # traducteur: Ceci est le titre de la boîte de dialogue
            # d'édition d'étape, de la forme <titre> à <lieu> à
            # <date>.
            etiq = _("<big><b>%(titre)s</b></big>\n<i>%(date)s %(lieu)s</i>")%{'titre':etape.titre,
                                                                                          'lieu':etape.lieu,
                                                                                          'date':etape.date}
            self.ui.get_object('etape_etiq').set_markup(etiq)
            for item in etape.items:
                self.items.append([item])
            self.items.append([""])
            self.ui.get_object('etape_preparation_champ').get_buffer().set_text(etape.preparation)
            self.ui.get_object('etape_activite_champ').get_buffer().set_text(etape.activite)


    def on_etape_modifier_clicked(self, bouton):
        d = self.ui.get_object('etape_dialog')
        d.run()
        d.hide()

    def on_etape_suppr_clicked(self, bouton):
        model, iter = self.get_etape_crt_iter()
        model.remove(iter)

    def on_etape_deleted(self, model, iter):
        iter = self.etapes.get_iter_first()
        etapes = []
        while iter is not None and model.iter_is_valid(iter):
            etape = model.get_value(iter, defs.COL_ETAPE)
            etapes.append(etape)
            iter = model.iter_next(iter)

        self.jeu.etapes = etapes
        self.sync(False)
            

    def on_etape_titre_edited(self, renderer, path, titre):
        model, iter = self.get_etape_crt_iter()
        etape = self.get_etape_crt()
        etape.titre = titre
        model.set(iter,
                  defs.COL_ET_TITRE, titre)
        self.sync(False)
        

    def on_etape_date_edited(self, renderer, path, date):
        model, iter = self.get_etape_crt_iter()
        etape = self.get_etape_crt()
        etape.date = date
        model.set(iter,
                  defs.COL_ET_DATE, date)
        self.sync(False)

    def on_etape_lieu_edited(self, renderer, path, lieu):
        model, iter = self.get_etape_crt_iter()
        etape = self.get_etape_crt()
        etape.lieu = lieu
        model.set(iter,
                  defs.COL_ET_LIEU, lieu)
        self.sync(False)

    def on_item_edited(self, renderer, path, item):
        """tiens compte de l'édition des items à apporter pour l'étape
        du jeu, une étape vide est supprimée, une étape vierge est
        toujours présente en fin de liste pour allonger la liste."""

        iter = self.items.get_iter(path)
        ancien = self.items.get_value(iter, defs.COL_ITEM)
        etape = self.get_etape_crt()

        if not ancien:
            if not item:
                return
            else:
                etape.items.append(item)
                self.items.set(iter, defs.COL_ITEM, item)
                self.items.append([""])
        else:
            etape.items.remove(ancien)
            if not item:
                self.items.remove(iter)
            else:
                etape.items.append(item)
                self.items.set(iter, defs.COL_ITEM, item)

        self.sync(False)

    def on_preparation_changed(self, tampon):
        etape = self.get_etape_crt()
        etape.preparation = tampon.get_text(tampon.get_start_iter(),
                                            tampon.get_end_iter())
        self.sync(False)
            
    def on_activite_changed(self, tampon):
        etape = self.get_etape_crt()
        etape.activite = tampon.get_text(tampon.get_start_iter(),
                                         tampon.get_end_iter())
        self.sync(False)



class Etape:
    def __init__(self,titre="Étape",date='jour/heure',lieu="",items=None,preparation="",activite=""):
        self.titre      = titre
        self.date       = date
        self.lieu       = lieu

        if items is None:
            self.items  = []
        else:
            self.items      = items
        self.preparation= preparation
        self.activite   = activite

    def createXML(self, doc):
        et = doc.createElement('etape')
        et.setAttribute('titre', self.titre)
        et.setAttribute('date', self.date)
        et.setAttribute('lieu', self.lieu)

        for item in self.items:
            e = doc.createElement('item')
            e.setAttribute('item', item)
            et.appendChild(e)

        e = doc.createElement('preparation')
        e.appendChild(doc.createTextNode(self.preparation))
        et.appendChild(e)

        e = doc.createElement('activite')
        e.appendChild(doc.createTextNode(self.activite))
        et.appendChild(e)

        return et
