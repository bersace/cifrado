# -*- coding: utf-8 -*-

# Cifrado - Chiffrer et imprimer les messages de grand-jeu
# Copyright © 2006-2007 Étienne Bersac — Tous droits réservés.

import gobject
import pango
import gtk

import  gettext
_ = gettext.gettext

from    .       import  codeur
from    .       import  codeurs
from    .       import  module
from    .       import  imprimeur
from    .       import  defs

def charger(el, jeu):
    f = el.getAttribute('dest').encode('utf-8')
    try:
        force = jeu.forces[f]
    except:
        force = jeu.forces.values()[0]
    dest = force
    lieu = jeu.lieux[el.getAttribute('lieu')]
    env = el.hasAttribute('envoyer')

    codeurs = []
    for e in el.getElementsByTagName('codeur'):
        codeurs.append(codeur.charger(e))

    substitutions = {}
    for e in el.getElementsByTagName('substitution'):
        substitutions[e.getAttribute('variable')] = e.getAttribute('valeur')

    return Emission(env, dest, lieu, codeurs, substitutions)

class Module(module.Module):
    def _init(self, cifrado):
        # chaine de chiffrage
        self.chaine = gtk.ListStore(gobject.TYPE_PYOBJECT,
                                    gobject.TYPE_STRING, # nom
                                    gobject.TYPE_STRING) # conf
        self.ui.get_object('codeur_liste').set_model(self.chaine)

        # configuration des colonnes de la chaîne de chiffrage
        liste = self.ui.get_object('codeur_liste')
        liste.get_selection().connect('changed', self.on_codeur_selection_changed)

        # codeur
        renderer = gtk.CellRendererCombo()
        renderer.set_property('editable', True)
        renderer.set_property('has-entry', False)
        renderer.set_property('text-column', defs.COL_COD_NOM)
        # liste des codeurs disponibles
        renderer.set_property('model', defs.codeurs)
        self.codeur_renderer = renderer
        renderer.connect('edited', self.on_codeur_edited)
        # traducteurs: titre de colonne
        colonne = gtk.TreeViewColumn(_('Chiffrement'), renderer,
                                     text=defs.COL_COD_NOM)
        colonne.set_expand(True)
        liste.append_column(colonne)

        # prop
        renderer = gtk.CellRendererText()
        # traducteurs: titre de colonne
        colonne = gtk.TreeViewColumn(_('Configuration'), renderer,
                                     text=defs.COL_COD_PROP)
        colonne.set_expand(True)
        liste.append_column(colonne)


        # Fenêtre de conversion.
        d = self.ui.get_object('conversion_dialog')
        d.add_buttons(gtk.STOCK_COPY, gtk.RESPONSE_APPLY,
                      gtk.STOCK_CLOSE, gtk.RESPONSE_CLOSE)
        c = gtk.clipboard_get(gtk.gdk.SELECTION_PRIMARY)
        self.ui.get_object('conversion_champ').get_buffer().add_selection_clipboard(c)

        

        # SUBSTITUTIONS
        self.substitutions = gtk.ListStore(gobject.TYPE_STRING,
                                           gobject.TYPE_STRING)
        
        liste = self.ui.get_object('substitutions_liste')
        liste.set_model(self.substitutions)

        # Variable
        renderer = gtk.CellRendererText()
        # traducteurs: titre de colonne
        colonne = gtk.TreeViewColumn(_('Variable'), renderer,
                                     text=defs.COL_SUB_VAR)
        colonne.set_expand(True)
        liste.append_column(colonne)

        # Valeur
        renderer = gtk.CellRendererText()
        renderer.set_property('editable', True)
        renderer.connect('edited', self.on_substitution_edited)
        # traducteurs: titre de colonne
        colonne = gtk.TreeViewColumn(_('Valeur'), renderer,
                                     text=defs.COL_SUB_VAL)
        colonne.set_expand(True)
        liste.append_column(colonne)




                            

    def ajouter_codeur(self, codeur):
        em = self.get_em_crt()
        if em is None:
            return

        return self.chaine.append([codeur,
                                   codeur.nom,
                                   codeur.get_prop_desc()])

    def changer_codeur(self, iter, classe):
        em = self.get_em_crt()
        if classe is codeurs.vigenere.Codeur:
            codeur = classe(em.dest)
        else:
            codeur = classe()

        cod = self.get_cod_crt()
        cods = self.get_em_crt().codeurs
        for c in cods:
            if cod is c:
                cods[cods.index(c)] = codeur

        self.chaine.set(iter,
                        defs.COL_COD,          codeur,
                        defs.COL_COD_NOM,      classe.nom,
                        defs.COL_COD_PROP,     codeur.get_prop_desc())


    def get_em_crt(self):
        iter = self.ui.get_object('em_dest_select').get_active_iter()
        if iter:
            dest = self.cifrado.modules['force'].forces.get_value(iter, defs.COL_FORCE)
            msg = self.cifrado.modules['message'].get_msg_crt()
            return msg.emissions[dest]
        else:
            return None
        

    def on_em_dest_select_changed(self, combo):
        """Selectionne l'émission correspondant au destinataire sélectionnée"""
        if self.jeu is None:
            return

        sync = self.jeu.sync

        self.chaine.clear()
        em = self.get_em_crt()
        if em is None:
            return

        for codeur in em.codeurs:
            self.ajouter_codeur(codeur)

        wids = ['em_env_champ']
        envc = self.ui.get_object('em_env_champ')
        if em:
            for wid in wids:
                self.ui.get_object(wid).set_sensitive(True)
            envc.set_active(em.env)
            self.select_combo(self.ui.get_object('em_lieu_select'), defs.COL_LIEU, em.lieu)
        else:
            for wid in wids:
                self.ui.get_object(wid).set_sensitive(False)
            self.ui.get_object('codeur_liste').set_model(None)
            envc.set_active(False)

        self.actualiser_substitutions()

        envc.toggled()
        self.sync(sync)

    def on_em_env_champ_toggled(self, bouton):
        em = self.get_em_crt()
        if em is None:
            return

        em.env = bouton.get_active()
        wids = ['em_lieu_select', 'chiffrement_exp', 'subs_exp',
                'codeur_nv_btn', 'codeur_prop_btn', 'codeur_conv_btn',
                'codeur_suppr_btn']
        for wid in wids:
            self.ui.get_object(wid).set_sensitive(em.env)
        self.sync(False)

    def on_em_lieu_select_changed(self, combo):
        iter = combo.get_active_iter()
        if iter is None:
            return
        lieu = combo.get_model().get_value(iter, defs.COL_LIEU)
        self.get_em_crt().lieu = lieu
        self.sync(False)

    # SUBSTITUTIONS
    def actualiser_substitutions(self):
        em = self.get_em_crt()
        if em is None:
            return
        
        # substitutions
        msg = self.cifrado.modules['message'].get_msg_crt()
        self.substitutions.clear()
        variables = msg.lister_variables()
        utile = len(variables) != 0
        exp = self.ui.get_object('subs_exp')
        exp.set_expanded(utile)

        if utile is True:
            for var in variables:
                self.substitutions.append([var, em.get_val(var)])
        

    def on_substitution_edited(self, renderer, path, valeur):
        iter = self.substitutions.get_iter(path)
        var = self.substitutions.get_value(iter, defs.COL_SUB_VAR)
        self.get_em_crt().set_val(var, valeur)
        self.substitutions.set_value(iter, defs.COL_SUB_VAL, valeur)
        self.sync(False)

    # CODEUR
    def get_cod_crt(self):
        model, iter = self.ui.get_object('codeur_liste').get_selection().get_selected()
        return model.get_value(iter, defs.COL_COD)
        

    def on_codeur_nv_btn_clicked(self, bouton):
        codeur = codeurs.aucun.Codeur()
        iter = self.ajouter_codeur(codeur)
        self.get_em_crt().codeurs.append(codeur)
        self.ui.get_object('codeur_liste').get_selection().select_iter(iter)
        self.sync(False)

    def on_codeur_selection_changed(self, selection):
        model, iter = selection.get_selected()
        wids = ['codeur_prop_btn', 'codeur_suppr_btn']
        sensitive = iter is not None
        for wid in wids:
            self.ui.get_object(wid).set_sensitive(sensitive)

        if sensitive:
            self.setup_codeur_prop()

    def on_codeur_prop_btn_clicked(self, bouton):
        d = self.ui.get_object('codeur_prop_dialog')
        d.show_all()
        d.run()
        d.hide()
        cod = self.get_cod_crt()
        model, iter = self.ui.get_object('codeur_liste').get_selection().get_selected()
        model.set(iter, defs.COL_COD_PROP, cod.get_prop_desc())
        self.sync(False)

    def on_codeur_conv_btn_clicked(self, bouton):
        b = self.ui.get_object('conversion_champ').get_buffer()
        em = self.get_em_crt()
        msg = self.cifrado.modules['message'].get_msg_crt()
        texte = em.substituer(msg.message, msg.lister_variables())
        texte = em.convertir(texte)
        b.set_text(texte)
        d = self.ui.get_object('conversion_dialog')
        r = d.run()
        d.hide()
        if r == gtk.RESPONSE_APPLY.__index__():
            c = gtk.clipboard_get(gtk.gdk.SELECTION_CLIPBOARD)
            b.select_range(b.get_start_iter(), b.get_end_iter())
            b.copy_clipboard(c)

    def on_codeur_suppr_btn_clicked(self, bouton):
        model, iter = self.ui.get_object('codeur_liste').get_selection().get_selected()

        # suppression effective
        cod = self.get_cod_crt()
        cods = self.get_em_crt().codeurs

        if len(cods) == 1:
            # on garde au minimum la liste des codeur égale à un
            # codeur nul
            self.changer_codeur(iter, codeurs.aucun.Codeur)
        else:
            for c in cods:
                if cod is c:
                    cods.remove(c)
            model.remove(iter)

        self.sync(False)

    def get_cod_classe_depuis_nom(self, nom):
        return defs.codeurs_classes[nom]

    def on_codeur_edited(self, renderer, path, texte):
        classe = self.get_cod_classe_depuis_nom(texte)
        iter = self.ui.get_object('codeur_liste').get_model().get_iter(path)
        em = self.get_em_crt()
        self.changer_codeur(iter, classe)
        self.setup_codeur_prop()
        self.sync(False)

    def setup_codeur_prop(self):
        cod = self.get_cod_crt()

        w = self.ui.get_object('codeur_prop_cont').get_child()
        if w:
            w.destroy()

        w = cod.get_prop_widget()

        if w is not None:
            self.ui.get_object('codeur_prop_cont').add(w)

        self.ui.get_object('codeur_prop_btn').set_sensitive(w is not None)





class Emission(imprimeur.Imprimeur):
    def __init__(self, env, dest, lieu, cods=None, subs=None, msg=None):
        self.msg = msg
        self.env = env
        self.dest = dest
        self.lieu = lieu
        if subs is None:
            subs = {}
        self.subs = subs

        if cods is None:
            self.codeurs = [codeurs.aucun.Codeur()]
        else:
            self.codeurs = cods

    def get_val(self, var):
        if var == _('$FORCE'):
            return self.dest.nom
        if var == _('$CHEF'):
            return self.dest.chef
        if var == _('$CLEF'):
            for cod in self.codeurs:
                if cod.__class__ is codeurs.vigenere.Codeur:
                    return cod.clef
            return self.dest.clef

        if not self.subs.has_key(var):
            self.set_val(var, "")
        return self.subs[var]

    def set_val(self, var, val):
        self.subs[var] = val

    def substituer(self, texte, variables):
        variables.extend(defs.prevars)

        for var in variables:
            texte = texte.replace(var, self.get_val(var))
        return texte

    def convertir(self, texte):
        for codeur in self.codeurs:
            texte = codeur.convertir(texte)

        return texte

    def get_dernier_codeur(self):
        return self.codeurs[len(self.codeurs)-1]

    def get_police(self):
        return pango.FontDescription(self.get_dernier_codeur().get_police())

    def get_interligne(self):
        il = 0
        for cod in self.codeurs:
            il+=cod.get_interligne()
        return il

    # impression
    def get_titre(self):
        # traducteurs: titre de la page de message : <titre> pour <destinataire>
        return _("Message « %(titre)s » pour « %(nom)s »")%{'titre':self.msg.titre,
                                                            'nom':self.dest.nom}

    def begin_print(self, op, contexte):
        op.set_job_name(self.get_titre())
        self.print_settings.set("output-uri", self.get_titre()+".pdf")

        op.set_n_pages(0)

    def paginate(self, op, contexte):
        if self.env:
            n = op.get_property('n-pages')
            if n <0:
                n = 0

            # toujours imprimer sur une page paire en duplex, ajouter
            # une page blanche si nécessaire.
            duplex = op.get_print_settings().get_duplex() != gtk.PRINT_DUPLEX_SIMPLEX
            titre = self.get_titre()
            if duplex and n%2:
                n+=1
                op.titre_pages.append(titre)
            # page de l'émission
            n+=1
            op.titre_pages.append(titre)

            # page blanche après si duplex
            if duplex:
                n+=1
                op.titre_pages.append(titre)
            op.set_n_pages(n)
        return True

    def draw_page(self, op, contexte, n):
        equipe = dest = self.dest
        dc = self.get_dernier_codeur()
        n = self.no

        police = dc.get_police()
        if not police:
            police = self.msg.exp.police
        if not police:
            police = "serif 9"

        ih = 48
        marge = 0
        cr = contexte.get_cairo_context()
        pg = contexte.create_pango_context()
        division = 9

        # Titres : on affiche nom de l'équipe, nom du message et
        # identifiant pour la maîtrise.
        titre = _("Message n°%(n)i : « %(titre)s » de « %(exp)s » à « %(dest)s » ; Déposer à %(lieu)s ; %(date)s")%{'n':n,
                                                                                                                    'titre':self.msg.titre,
                                                                                                                    'exp':self.msg.exp.nom,
                                                                                                                    'dest':dest.nom,
                                                                                                                    'lieu':self.lieu.nom,
                                                                                                                    'date':self.msg.date}
        w, hh = self.impr_text(contexte, titre, font='serif bold 8')

        # on affiche uniquement l'identifiant pour les scouts
        id = "%i"%(n)
        layout = self.layout_text(contexte, id,
                                  font='sans normal 7',
                                  align=pango.ALIGN_RIGHT)
        w, h = layout.get_size()
        self.impr_layout(contexte, layout,
                         y=contexte.get_height() - 2*h/pango.SCALE)

        # séparateur
        cr.new_path()
        cr.save()
        cr.set_source_rgb(.5, .5, .5)
        cr.move_to(0,
                   contexte.get_height()/division
                   +self.page_setup.get_bottom_margin(gtk.UNIT_POINTS))
        cr.rel_line_to(contexte.get_width(), 0)
        cr.set_line_width(.5)
        cr.set_dash([2.,8.])
        cr.stroke()
        cr.restore()

        # BANDEAU CHEF
        cr.rectangle(0, 0,
                     contexte.get_width(),
                     contexte.get_height()/division
                     +self.page_setup.get_bottom_margin(gtk.UNIT_POINTS)-2)
        cr.save()
        cr.clip()

        # chiffrements
        il = 0                  # interligne
        wc = 0
        if dc.__class__ is not codeurs.aucun.Codeur:
            # traducteurs: titre au dessus de la liste des codeurs
            # utilisé dans le bandeau de chef.
            hw,h = self.impr_text(contexte, _("Chiffré avec"),
                                  font='sans bold 8',
                                  y=hh,
                                  align=pango.ALIGN_RIGHT)

            mw = 0
            layouts = []
            for codeur in self.codeurs:
                texte = codeur.nom
                prop = codeur.get_prop_desc()
                if prop is not None:
                    texte+=' ( '+prop+' )'

                layout = self.layout_text(contexte, texte,
                                          font='serif 7')
                w,h = layout.get_size()
                mw = max(mw, w/pango.SCALE)

                layouts.append(layout)

            il = 3
            wc = 2*il+max(hw,mw)    # largeur de colonne
            y = hh + il + h/pango.SCALE + il

            for layout in layouts:
                w,h = self.impr_layout(contexte, layout,
                                       x=contexte.get_width()-mw,y=y)
                y+=h+il

            # séparateur vertical
            cr.new_path()
            cr.save()
            cr.set_source_rgb(.5, .5, .5)

            cr.move_to(contexte.get_width()-wc-il,hh)
            cr.rel_line_to(0, y-hh)
            cr.set_line_width(.5)
            cr.stroke()
            cr.restore()

        texte = self.substituer(self.msg.message,
                                self.msg.lister_variables())

        # Original
        self.impr_text(contexte, texte,
                       font='serif 7',
                       y=int(1.5 * hh),
                       width=contexte.get_width()-(il+wc),
                       justify=True)

        cr.restore()         # fin du bandeau chef

        # Chiffré
        layout = self.layout_text(contexte, self.convertir(texte),
                                  font=police,
                                  justify=True)
        w,h = layout.get_size()
        layout.set_spacing(int(self.get_interligne()* h / layout.get_line_count()))
        self.impr_layout(contexte, layout,
                         y=contexte.get_height()/division
                         +self.page_setup.get_bottom_margin(gtk.UNIT_POINTS)
                         +self.page_setup.get_top_margin(gtk.UNIT_POINTS))


    # archive
    def createXML(self, doc):
        e = doc.createElement('emission')
        if self.env:
            e.setAttribute('envoyer', 'envoyer')
        e.setAttribute('lieu', self.lieu.nom)
        e.setAttribute('dest', self.dest.nom)

        for codeur in self.codeurs:
            e.appendChild(codeur.createXML(doc))

        for var in self.subs:
            if not self.get_val(var) == "":
                s = doc.createElement('substitution')
                s.setAttribute('variable', var)
                s.setAttribute('valeur', self.get_val(var))
                e.appendChild(s)

        return e
